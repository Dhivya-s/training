package com.java.training.core.Internationalization;

/*
Requirement:
    To print the following pattern of the Date and Time using SimpleDateFormat.
    "yyyy-MM-dd HH:mm:ssZ"

Entity:
    ISODateDemo

Method Signature:
    public static void main(String[] args)

Jobs to be Done:
    1) Declare the ISO date pattern as string.
    2) Assign that pattern for Simple Date Format.
    3) Get the current date as that particular pattern.

Pseudo Code:

class ISODateDemo {
    public static void main(String[] args) {
        Date date = new Date();
        String pattern = "yyyy-MM-dd HH:mm:ssZ";
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date1 = simpleDateFormat.format(date);
        
        System.out.println(date1);                                     
    }
}
 */
import java.text.SimpleDateFormat;
import java.util.Date;

public class ISODateDemo {

    public static void main(String[] args) {
        Date date = new Date();
        String pattern = "yyyy-MM-dd HH:mm:ssZ";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date1 = simpleDateFormat.format(date);
        System.out.println(date1);
    }
}
