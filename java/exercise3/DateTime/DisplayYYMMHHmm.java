package com.java.training.core.DateTime;

/*
Requirement:
    Write a Java program to get and display information (year, month, day, hour, minute) of a default calendar

Entity:
    DisplayYYMMHHmm 

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create instance for Calendar class and store it in variable calendar
    2. Get the year month date hour minute By accessing calendar and print it.

Pseudo code:

public class DisplayYYMMHHmm {

    public static void main(String[] args) {
        
        // Create a default calendar
        Calendar calendar = Calendar.getInstance();
        
        // Get and display information of current date from the calendar:
        System.out.println();
        System.out.println("Year: " + calendar.get(Calendar.YEAR));
        System.out.println("Month: " + calendar.get(Calendar.MONTH));
        System.out.println("Day: " + calendar.get(Calendar.DATE));
        System.out.println("Hour: " + calendar.get(Calendar.HOUR));
        System.out.println("Minute: " + calendar.get(Calendar.MINUTE));
        System.out.println();
    }
}

 */

import java.util.Calendar;

public class DisplayYYMMHHmm {

    public static void main(String[] args) {
        
        // Create a default calendar
        Calendar calendar = Calendar.getInstance();
        
        // Get and display information of current date from the calendar:
        System.out.println();
        System.out.println("Year: " + calendar.get(Calendar.YEAR));
        System.out.println("Month: " + calendar.get(Calendar.MONTH));
        System.out.println("Day: " + calendar.get(Calendar.DATE));
        System.out.println("Hour: " + calendar.get(Calendar.HOUR));
        System.out.println("Minute: " + calendar.get(Calendar.MINUTE));
        System.out.println();
    }
}

