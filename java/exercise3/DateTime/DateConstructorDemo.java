package com.java.training.core.DateTime;

//Java program to demonstrate constructors of Date 
import java.util.Date; 

public class DateConstructorDemo { 

 public static void main(String[] args) { 
     Date date1 = new Date(); 
     System.out.println("Current date is " + date1); 
     
     //return current date
     Date date2 = new Date(8640000L);
     
     //return by removing the millisecond from jan 1 1970 
     System.out.println("Date represented is "+ date2 );
 } 
} 
