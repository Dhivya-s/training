package com.java.training.core.DateTime;

/*
Requirement:
    To write a java code to print my age
    
Entity:
    CalculateAgeDemo
    
Function declaration:
    public static void man(String[] args)
    
Jobs to be done:
    1. Create two instances of LocalDate as dateOfBirth 
       and age to store your date of birth and current date.
    2. Create an instance for Period as myAge and calculate difference of dateOfBirth and
       age and stored the difference in myAge.
    3. Print myAge.
    
Pseudo code:

class CalculateAgeDemo {
    
    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(2001, 1, 11);
        LocalDate age = LocalDate.now();
        Period myAge = Period.between(dateOfBirth, age);
        System.out.println("I am " + myAge.getYears() + " " + "years old");
    }
}
*/

import java.time.LocalDate;
import java.time.Period;

public class CalculateAgeDemo {
    
    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(2000, 07, 11);
        LocalDate age = LocalDate.now();
        Period myAge = Period.between(dateOfBirth, age);
        System.out.println("I am " + myAge.getYears() + " " + "years old");
    }
}

