package com.java.training.core.DateTime;

/*
Requirement:
    To write a Java program to get the dates 1 year before and after today.
      
Entity:
    LocalDateDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. An instance is created for Calendar calendar that gets the calendar using current time zone
       and locale of the system.
    2. Print Date before 1 year and Date after 1 year .
    
Pseudo code:

class BeforeAndAfterYearDemo {
    
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calendar.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("Date after 1 year is: " + " " + calendar.getTime());
    }
}
*/
import java.util.Calendar;

public class BeforeAndAfterAYearDemo {
    
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        calendar.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calendar.getTime());
        
        calendar.add(Calendar.YEAR, 2);
        System.out.println("Date after 1 year is: " + " " + calendar.getTime());
    }
}
