package com.java.training.core.DateTime;

/*
Requirement:
    1. To check whether the current or given year is leap or not.
    2. To find the length of the year.
    
Entity:
    LeapYearOrNotDemo

Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:

    1.Get the input year from user
    2.Create instance and store the given year in variable givenYear.
    3.Check whether the givenYear is leap year,
           3.1) If it is a leap year, print "the given year is leap year"
           3.2) If it is not a leap year, print "the given year is not a leap year".
    4. Find the length of the givenYear and store it in lengthOfCurrentYear.
           4.1) print the length of the current year.
           
Pseudo code:

class LeapYearOrNotDemo {

   public static void main(String[] args) {
        System.out.println("Enter the year");
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        Year givenYear = Year.of(year);
        if (givenYear.isLeap() == true) {
            System.out.println("The given year is a leap year");
        } else {
            System.out.println("The given year is not a leap year");
        }
        
        int lengthOfGivenYear = givenYear.length();
        System.out.println("The length of the given year is: " + " " + lengthOfGivenYear);
        scanner.close();
    }

}        
*/
import java.time.Year;
import java.util.Scanner;

public class LeapYearOrNotDemo {
    
    public static void main(String[] args) {
        System.out.println("Enter the year");
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        Year givenYear = Year.of(year);
        if (givenYear.isLeap() == true) {
            System.out.println("The given year is a leap year");
        } else {
            System.out.println("The given year is not a leap year");
        }
        
        int lengthOfGivenYear = givenYear.length();
        System.out.println("The length of the given year is: " + " " + lengthOfGivenYear);
        scanner.close();
    }

}

