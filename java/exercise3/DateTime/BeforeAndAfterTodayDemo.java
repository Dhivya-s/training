package com.java.training.core.DateTime;

/*
Requirement:
    Write a Java program to get the dates 10 days before and after today.
    1.Using LocalDate
    2.Using Calendar
Entity:
    BeforeAndAfterTodayDemo
    
Function Declaration:
    public class BeforeAndAfterTodayDemo

Jobs To Be Done :
    // using Local Date
    1.Create a instance for LocalDate and store the current date in variable (today)
    2.Print the 10 days before this day using plusDay method
    3.Print the 10 days after this day using plusDay method
    
    // Using calendar:
    1.An instance is created for Calendar calendar that gets the calendar using current time zone
      and locale of the system.
    2.Now the date 10 days before the current date and after the current date is printed.

Pseudo code :

public class BeforeAndAfterTodayDemo {
    
    public static void main(String[] args) {
        LocalDate today = LocalDate.now(); 
        System.out.println("\nCurrent Date: "+today);
        System.out.println("10 days before today will be "+today.plusDays(-10));
        System.out.println("10 days after today will be "+today.plusDays(10)+"\n");
        
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        calender.add(Calendar.DATE, -10);
        System.out.println("The day 10 days before the current day is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, 20);
        System.out.println("The day 10 days after the current day is: " + " " + calender.getTime());
    }
}
*/
import java.time.LocalDate;
import java.util.Calendar;

public class BeforeAndAfterTodayDemo {
    
    public static void main(String[] args) {
        
        // using LocalDate
        LocalDate today = LocalDate.now(); 
        System.out.println("\nCurrent Date: "+today);
        System.out.println("10 days before today will be "+today.plusDays(-10));
        System.out.println("10 days after today will be "+today.plusDays(10)+"\n");
        
        // using calendar
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        calendar.add(Calendar.DATE, -10);
        System.out.println("The day 10 days before the current day is: " + " " + calendar.getTime());
        
        calendar.add(Calendar.DATE, 20);
        System.out.println("The day 10 days after the current day is: " + " " + calendar.getTime());
        
    }

}
