package com.java.training.core.DateTime;



//To demonstrate java timer class methods example
//import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerMethodsDemo {
    
    public static void main(String[] args) {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
                for(int i = 0; i <= 10; i++ ) {
                    System.out.println( "number : " + i);
                    timer.cancel();
                } 
                System.out.println("stop" + " purge value of Task : " + timer.purge());
           }
        };
      timer.schedule(task,100000,10000); // timer schedule( task,first date ,delay)
     //timer.scheduleAtFixedRate(task,500,1000); //scheduleAtFixedRate( TimerTask task ,long delay ,long period)
     //timer.scheduleAtFixedRate(task,new Date(),1000);// scheduleAtFixedRate( TimerTask task, Date firstTime, long period)
    }
}