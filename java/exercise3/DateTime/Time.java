package com.java.training.core.DateTime;

public class Time {
    
    public static void main(String[] args) {
        
     // starting time 
        long start = System.currentTimeMillis(); 
        System.out.println(  "staring time in millsecond  :  " + start + "ms");
        
     // start of function 
        countfunction(100); 
        
     // end of function 

     // ending time 
        long end = System.currentTimeMillis(); 
        System.out.println("End  time in millsecond :  " + end + "ms");
        System.out.println("Counting to 100 takes " + (end - start) + "ms"); 
    } 
    // A dummy function that runs a loop x times 
    public static void countfunction(long x) { 
        for (long i = 0; i < x; i++) {
             System.out.println(i);
        }
        
    } 
}
//1 millisecond = 1000 microsecond
//1000 millisecond = 1 second
