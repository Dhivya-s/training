package com.java.training.core.DateTime;

/*
Requirement:
    To find maximum and minimum value of the year, Month, week and day of a default calendar.
    
Entity:
    MaximumAndMinimumDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create an instance of a Calendar as calendar that gets the calendar using current time zone
    2. create a integer variable actualMaxYear to store the actual maximum year of the default calendar.
    3. create a integer variable actualMaxMonth to store the actual maximum month of the default calendar.
    4. create a integer variable actualMaxWeek to store the actual maximum week of the default calendar.
    5. create a integer variable actualMaxDay to store the actual maximum day of the default calendar.
    6. create a integer variable actualMaxYear to store the actual minimum year of the default calendar.
    7. create a integer variable actualMaxMonth to store the actual minimum month of the default calendar.
    8. create a integer variable actualMaxWeek to store the actual minimum week of the default calendar.
    9. create a integer variable actualMaxDay to store the actual minimum day of the default calendar.
    10.Print Actual Maximum Year,Actual Maximum Month ,Actual Maximum Week ,Actual Maximum Date.
    
    
Pseudo code:

public class MaximumAndMinimumDemo {
    
    public static void main(String[] args)
    {
     // Create a default calendar
        Calendar calendar = Calendar.getInstance();
        System.out.println("\nCurrent Date and Time:" + calendar.getTime());     
        int actualMaxYear = calendar.getActualMaximum(Calendar.YEAR);
        int actualMaxMonth = calendar.getActualMaximum(Calendar.MONTH);
        int actualMaxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        int actualMaxDate = calendar.getActualMaximum(Calendar.DATE);
        
        System.out.println("Actual Maximum Year: "+actualMaxYear);
        System.out.println("Actual Maximum Month: "+actualMaxMonth);
        System.out.println("Actual Maximum Week of Year: "+actualMaxWeek);
        System.out.println("Actual Maximum Date: "+actualMaxDate);
    }
}
*/
import java.util.Calendar;

public class MaximumAndMinimumDemo {
    
    public static void main(String[] args)
    {
     // Create a default calendar
        Calendar calendar = Calendar.getInstance();
        System.out.println("\nCurrent Date and Time:" + calendar.getTime());     
        int actualMaxYear = calendar.getActualMaximum(Calendar.YEAR);
        int actualMaxMonth = calendar.getActualMaximum(Calendar.MONTH);
        int actualMaxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        int actualMaxDate = calendar.getActualMaximum(Calendar.DATE);
        
        System.out.println("Actual Maximum Year: "+actualMaxYear);
        System.out.println("Actual Maximum Month: "+actualMaxMonth);
        System.out.println("Actual Maximum Week of Year: "+actualMaxWeek);
        System.out.println("Actual Maximum Date: "+actualMaxDate);
    }
}
