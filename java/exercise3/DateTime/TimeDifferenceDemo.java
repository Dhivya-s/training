/*
Requirement:
    To find the difference between the times using timer class.
    
Entity:
    TimeDifferenceDemo

Function Declaration:
    public static void main(String[] args)
   
    
Jobs to be done:
    1. Declare the long variable startTime which gets the System time in milliseconds.
       1.1) Print that starting time.
    2. Using loop Print initial value 10 times
       2.1)After printing, get the current system timing in millisecond and store it in the endTime.
    3. Print the difference between startTime and endTime.
    
Pseudo code:

class TimeDifferenceDemo {
    
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        
        for (int initialValue = 0; initialValue < 100 ; initialValue++) {
            System.out.println(time);
        }
        
        long endTime = System.currentTimeMillis();;
        System.out.println("THe difference between the time is: " + (startTime - endTime) + "  " + "ms");
    }
}
*/
package com.java.training.core.DateTime;

public class TimeDifferenceDemo {
    
    public static void main(String[] args) {
        long start = System.currentTimeMillis(); 
        System.out.println(  "staring time in millsecond  :  " + start + "ms");
            
        for (long initialValue = 0; initialValue < 100; initialValue++) {
            System.out.println(initialValue);
        }
      
        long end = System.currentTimeMillis(); 
        System.out.println("End  time in millsecond :  " + end + "ms");
        System.out.println("Counting to 100 takes " + (end - start) + "ms"); 
    } 
}
