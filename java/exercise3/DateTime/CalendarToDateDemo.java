/*
Requirement:
    How do you convert a Calendar to Date and vice-versa with example?
   
Entity:
    CalendarToDateDemo
 
Method Signature:
    public static void main(String[] args)
  
Jobs To Be Done:
    1) Create  Calendar class and  get the instance time
       1.1) And also reference is created for Date class.
    2.Using the calendar object get the time 
       2.1) Print the Date with access from Date class and Calendar class.

Pseudo Code:

class CalendarToDate {

    public static void main(String[] args) {
    //Access the calendar
    Calendar calendar = Calendar.getInstance();
    Date date = calendar.getTime();
    System.out.println(date);
        }
  }
*/
package com.java.training.core.DateTime;

import java.util.Date;
import java.util.Calendar;

public class CalendarToDateDemo {
    
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        System.out.println(date);
    }
}
