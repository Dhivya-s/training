/* 
Requirement:
    To Write a Java program to copy all of the mappings from the specified map to another map
    To Write a Java program to test if a map contains a mapping for the specified key?
    To Count the size of mappings in a map?
    To Write a Java program to get the portion of a map whose keys range from a given key to another key?
    
Entity:
    MapDemo1
    
Function Declaration:
    public static void main(String[] args) 
    put()
    putAll()
    contains key()
    size()
    subMap()

Jobs to be Done:
    1.create a class MapDemo1 and create a map 
    2.using put() add keys and values to the map
    3.copy all of the mappings from the specified map to another map
    4.Then print the copied map values
    5.To check the value is present or not,if the key is present
      get the value of key using containsKey method and print the values
      
pseudo code:
public class MapDemo1 {
    
    public static void main(String[] args) {
        HashMap hashmap 
        HashMap hashmap1 
        hashmap.put(1, "11");
        System.out.println("Values in first map: " + hashmap);
        hashmap1.put(5, "15");
        System.out.println("the values of second map : " +hashmap1);
        // copy all of the mappings from the specified map to another map
        hashmap1.putAll(hashmap);
        System.out.println("the total values of map : " + hashmap1);
        // To check whether the key is present or not
        
        if (hashmap1.containsKey(1)) {
            System.out.println(" yes " + hashmap1.get(1));
        } else {
            System.out.println("no");
        }
        
        // using Size()
        System.out.println("size of first map: " + hashmap.size());
        
        //To get the portion of a map whose keys range from a given key to another key
        TreeMap treemap 
        SortedMap  subtreemap 

        // Put elements to the map 
        treemap.put(10, "Red");
        System.out.println("Orginal TreeMap content: " + treemap);
        subtreemap = treemap.subMap(20, 40);
        System.out.println("Sub map key-values: " + subtreemap);
        
      
    }
}
*/

package com.java.training.core.map;

import java.util.*;

public class MapDemo1 {
	
    public static void main(String[] args) {
        HashMap<Integer,String> hashmap = new HashMap<Integer,String>();
        HashMap<Integer,String> hashmap1 = new HashMap <Integer,String>();
        hashmap.put(1, "11");
        hashmap.put(2, "12");
        hashmap.put(3, "13");
        System.out.println("Values in first map: " + hashmap);
        
        hashmap1.put(4, "14");
        hashmap1.put(5, "15");
        System.out.println("the values of second map : " +hashmap1);
        
        // copy all of the mappings from the specified map to another map
        
        hashmap1.putAll(hashmap);
        System.out.println("the total values of map : " + hashmap1);
        
        // To check whether the key is present or not
        
        if (hashmap1.containsKey(1)) {
            System.out.println(" yes " + hashmap1.get(1));
        } else {
            System.out.println("no");
        }
        
        // using Size()
        System.out.println("size of first map: " + hashmap.size());
        
        //To get the portion of a map whose keys range from a given key to another key
        TreeMap < Integer, String > treemap = new TreeMap < Integer, String > ();
        SortedMap < Integer, String > subtreemap = new TreeMap < Integer, String > ();

        // Put elements to the map 
        treemap.put(10, "Red");
        treemap.put(20, "Green");
        treemap.put(30, "Black");
        treemap.put(40, "White");
        treemap.put(50, "Pink");
        System.out.println("Orginal TreeMap content: " + treemap);
        subtreemap = treemap.subMap(20, 40);
        System.out.println("Sub map key-values: " + subtreemap);
        
      
    }
}