package com.java.training.core.Serialization;

/*
What will happen if one of the member in the class does not implement serializable interface?
 
Explanation :

When you try to serialize an object which implements Serializable interface, incase if the object
includes a reference of an non serializable object then NotSerializableException will be thrown.
 */
