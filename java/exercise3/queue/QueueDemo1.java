/*
Requirement:
    Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
    add at least 5 elements
    remove the front element
    search a element in stack using contains key word and print boolean value value
    print the size of stack
    print the elements using Stream 
    
Entity:
    QueueDemo1 

Function Declaration:
    public static void main(String[] args)
    size()
    remove()
    stream()
    add()
    
Jobs to be done:
    1. Create a package com.java.training.stack.
    2. import the Queue, LinkedList, PriorityQueue and Stream packages.
    3. Declare the class QueueDemo1 as public.
    4. Create an object string for the Queue class of String type and add elements and 
       print it.
    5. Now print the removed element and create a value of type boolean and print the result.
    6. Print the size of the queue and print the elements using Stream.
    7. Use the same procedure for the PriorityQueue and print the results.

pseudo code:

public class QueueDemo1 {
    
    public static void main(String[] args) {
        Queue  LinkedList<>();
        Queue  PriorityQueue<>();
        linkedlist.add("bike");
        // added elements
        
        priorityqueue.add(10);
       
        System.out.println(linkedlist);
       
        //remove front element
         System.out.println(" remove first element:  " + linkedlist.remove());
        
        // to search key 
        
        boolean value1 = priorityqueue.contains(30);
        boolean value2 = linkedlist.contains("car");//and print it
        // to find size
        
        System.out.println(linkedlist.size());
        System.out.println("size of  priority queue " + priorityqueue.size());
        Stream  stream 
        Stream  stream 
        System.out.println("the values of priority queue is ");
        stream1.forEach(element-> System.out.println(element));
        System.out.println("the values of linked list is ");
        stream2.forEach(element-> System.out.println(element));
        
        
    }
}

    
   
 */

package com.java.training.core.queue;

import java.util.*;

import java.util.stream.Stream;

public class QueueDemo1 {
    
    public static void main(String[] args) {
        Queue<String> linkedlist = new LinkedList<>();
        Queue<Integer> priorityqueue = new PriorityQueue<>();
        linkedlist.add("bike");
        linkedlist.add("car");
        linkedlist.add("bus");
        linkedlist.add("cycle");
        linkedlist.add("flight");
        
        // added elements
        
        priorityqueue.add(10);
        priorityqueue.add(20);
        priorityqueue.add(30);
        priorityqueue.add(40);
        priorityqueue.add(50);
        System.out.println(" before removing first element: " + linkedlist);
       
        //remove front element
        
        System.out.println(" remove first element:  " + linkedlist.remove());
        System.out.println(" before removing first element : " + priorityqueue);
        System.out.println(" remove first element" + priorityqueue.remove());
        
        // to search key 
        
        boolean value1 = priorityqueue.contains(30);
        boolean value2 = linkedlist.contains("car");
        System.out.println("the boolean value is " + value1);
        System.out.println("the boolean value is " + value2);
        
        // to find size
        
        System.out.println(" size of linkedlist" + linkedlist.size());
        System.out.println("size of  priorityqueue " + priorityqueue.size());
        
        Stream<Integer> stream1 =  priorityqueue.stream();
        Stream<String> stream2 =  linkedlist.stream();
        System.out.println("the values of priorityqueue ia ");
        stream1.forEach(element-> System.out.println(element));
        System.out.println("the values of linked list ia ");
        stream2.forEach(element-> System.out.println(element));
        
        
    }
}

	