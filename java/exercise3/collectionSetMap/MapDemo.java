/*
 Requirement:
 To demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() )

 Entity:
    MapDemo
    
Function Declaration :
    public static void main(String[] args )
    pulll()
    remove()
    replace()
    containsValue()

Jobs to be done :
    1.put values in map
    2.replace values in map
    3.remove values in map 
    4.check the value in map using contains value 
    5.print the values 

pseudocode:
    public class MapDemo {
    public static void main(String[] args ) {
        HashMap map // using hashMap print the keys and values
        map.put("1", "A");
        //add keys and values to map
        //contains value or not return in true or false 
        System.out.println(" Is the word is present ?  " + map.containsValue("A"));
        map.replace("1", "AA");
        //replace value
        map.remove("3");
        //remove key
        System.out.print("the final value is : " + map);
        
    }
}
*/

package com.java.training.core.collectionSetMap;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {
    
    public static void main(String[] args ) {
        Map <String ,String> map = new HashMap<>();
        map.put("1", "A");
        map.put("2", "B");
        map.put("3", "C");
        System.out.print("the key values are :  " + map);
        System.out.println(" Is the word is present ?  " + map.containsValue("A"));
        map.replace("1", "AA");
        map.remove("3");
        System.out.print("the finnial value is : " + map);
        
    }
}


