/*
 Requirement 
   demonstrate program explaining basic add and traversal operation of linked hash set
 
Entity:
    LinkedsetTraversalDemo

Function Declaration :
    public static void main(String[] args);
    add()
    
Jobs To Be Done :
    Add elements to the LinkedHashSet
    Remove elements to the hashSet
    using for each print the elements

pseudocode:
public class SetDemo {
    public static void main(String[] args) {
        LinkedHashSet linkedhashset
        //use LinkedHashset and add elements
        set.add("yoke");
        Iterator 
        //use iterator and print values
         while (value.hasNext()) { 
            System.out.println(value.next()); 
        } 
        
    }
}        
 */

package com.java.training.core.collectionSetMap;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedsetTraversalDemo {
    public static void main(String[] args) {
        Set<String> set = new LinkedHashSet<>();
        set.add("latha");
        set.add("Dhivya");
        set.add("enjoy");
        
        Iterator<String> value = set.iterator();
        System.out.println("The iterator values are: "); 
        while (value.hasNext()) { 
            System.out.println(value.next()); 
        } 
        
    }

}
