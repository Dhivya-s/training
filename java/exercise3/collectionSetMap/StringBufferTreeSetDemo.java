/*
Requirements:
    To demonstrate insertions and string buffer in tree set. 
     
Entities:
    public class StringBufferTreeSetDemo 
      
Function Declaration:
    public static void main(String[] args)
      
Jobs To Done:
    1. implements Comparator 
    2. Create the object tree set type as StringBuffer as Generic type.
    3. Add the string values.
    4. Prints the output.

Pseudocode:
public class StringBufferTreeSetDemo {
    
    private static class Demo implements Comparator<StringBuffer> {
        public static void main(String[] args) {
        StringBuffer one = new StringBuffer("one");
        Set<StringBuffer> set =new TreeSet<StringBuffer>(new Demo());
        set.add(one);
        System.out.println("The values are: "+ set);
    }
}
*/



package com.java.training.core.collectionSetMap;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class StringBufferTreeSetDemo {
    
    private static class Demo implements Comparator<StringBuffer> {
    @Override
        public int compare(StringBuffer word1, StringBuffer word2) {
            return word1.toString().compareTo(word2.toString());

        }

    }


    public static void main(String[] args) {
        StringBuffer one = new StringBuffer("one");
        StringBuffer  two = new StringBuffer("two");
        StringBuffer three = new StringBuffer("three");
        Set<StringBuffer> set =new TreeSet<StringBuffer>(new Demo());
        set.add(one);
        set.add(two);
        set.add(three);
        System.out.println("The values are: "+ set);
    }
}
