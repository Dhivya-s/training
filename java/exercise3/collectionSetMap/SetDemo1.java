/*
 Requirement 
    java program to demonstrate adding elements, displaying, removing, and iterating in hash set
 
Entity:
    SetDemo1

Function Declaration :
    public static void main(String[] args);
    add()
    remove()
Jobs To Be Done :
    Add elements to the hashSet
    Remove elements to the hashSet
    using for each print the elements

pseudocode:
public class SetDemo {
    public ststic void main(String[] args) {
        HashSet hashset
        //use hashset and add elements
        hashset.add("yoke");
        hashset.remove("yoke");
        Iterator 
        //use iterator and print values
         while (value.hasNext()) { 
            System.out.println(value.next()); 
        } 
        
    }
}        
 */
package com.java.training.core.collectionSetMap;

import java.util.HashSet;
import java.util.Iterator;

public class SetDemo1 {
    
    public static void main(String[] args) {
        
        HashSet<String> hashset = new HashSet<String>();
        hashset.add("sai");
        hashset.add("ram");
        hashset.add("ramji");
        System.out.println("Displaying set values " + hashset);
        
        hashset.remove("ramji");
        Iterator<String> value = hashset.iterator();
        System.out.println("The iterator values are: "); 
        while (value.hasNext()) { 
            System.out.println(value.next()); 
        } 
        
    }
    
}

