/*
Requirement :
    .demonstrate linked hash set to array() method in java

Entity:
    LinkedHashSetArray

Function Declaration :
    public static void main(String[] args)

Jobs to be Done :
   1.Add values to hashSet
   2.convert to array using toArray()
   3 Using for loop Print the the array
 
pseudocode:
public class LinkedHashSetArray {
    
    public static void main(String[] args) {
    
        LinkedHashSet<String> set = new LinkedHashSet<String>(); 
        set.add("fan");
        // Creating the array and using toArray() 
        Object[] array = set.toArray(); 
        for (int number = 0; number< array.length; number++) 
            System.out.println(array[number]); 
    } 
} 
*/

package com.java.training.core.collectionSetMap;

import java.util.LinkedHashSet;

public class LinkedHashSetArray {
    
    public static void main(String[] args) {
        LinkedHashSet<String> set = new LinkedHashSet<String>(); 
        set.add("fan");
        set.add("tv");
        set.add("bulb");
        System.out.println("The LinkedHashSet: " + set); 

         // Creating the array and using toArray() 
         Object[] array = set.toArray(); 

         System.out.println("The array is:"); 
         for (int number = 0; number< array.length; number++) 
         System.out.println(array[number]); 
    } 
} 




