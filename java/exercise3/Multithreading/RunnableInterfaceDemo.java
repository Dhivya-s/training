package com.java.training.core.Multithreading;
/*
Requirement:
    Write a program of performing two tasks by two threads that implements Runnable interface.
    
Entity:
    RunnableInterfaceDemo

Function Declaration:
     public void run() 
     public static void main(String[] args) 

Jobs to be Done:
     1. Create a Class RunnableInterfaceDemo which implements runnable
     2. Create a object for RunnableInterfaceDemo class 
     3. invoke Thread class and create object and invoke start method by using the object
        3.1) start method invoke the run method in RunnableInterfaceDemo class
        3.2) Print the result

Pseudo code :

    public class RunnableInterfaceDemo implements Runnable {
    
    public void run() {
        System.out.println("runnable interface");
    }
    public static void main(String[] args) {
         RunnableInterfaceDemo demo1 = new RunnableInterfaceDemo();
         Thread demo2 = new Thread(demo1);
         demo2.start();
    }
}
       

 */
public class RunnableInterfaceDemo implements Runnable {
    
    public void run() {
        System.out.println("runnable interface");
    }
    public static void main(String[] args) {
         RunnableInterfaceDemo demo1 = new RunnableInterfaceDemo();
         Thread demo2 = new Thread(demo1);
         Thread demo3 = new Thread(demo1);
         demo2.start();
         demo3.start();
         System.out.println("Thread names are following:");
         System.out.println(demo2.getName());
    }
}
