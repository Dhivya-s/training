package com.java.training.core.Multithreading;
/*
Requirement:
    Write a program which implement multiThreading using sleep(), setPriority(), getPriorty(), Name(), getId() methods.

Entity:
    MultitreadingDemo

Function Declaration:
    public static void main(String[] args);
    sleep()
    setPriority()
    getPriorty()
    Name()
    getId()

Jobs To Be Done:
    1. Create the class MultithreadingDemo and extends from Thread Class
    2. invoke the run method From class Thread
    3. Use for loop and check if the number is greater than one 
       3.1) if the condition satisfied use try block 
       3.2) invoke the run method using start method
       3.3) if any error come 
       3.4) catch the error using catch block and print it 
    4. Invoke the run method using object of MultithreadingDemo class 
       and Print the result.
       
Pseudo code:

public class MultithreadingDemo extends Thread {
    public void run() {
        for( int number = 1; number < 5; number++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
            System.out.println(number);
        }
    }
    public static void main(String[] args ) {
        MultithreadingDemo demo = new MultithreadingDemo();
        demo.setName("Thread");
        demo.getName();
        demo.getId();
        setPriority(Thread.MAX_PRIORITY);
        getPriority();
        demo.start();  
    }
}
 */
public class MultithreadingDemo extends Thread {
    public void run() {
        for( int number = 1; number < 5; number++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
            System.out.println("the values are : \n " + number);
        }
        System.out.println("Priority of thread is: "+Thread.currentThread().getPriority()); 
    }
    public static void main(String[] args ) {
        MultithreadingDemo demo = new MultithreadingDemo();
        demo.setPriority(Thread.MAX_PRIORITY);
        System.out.println("the name of the running thread is :\n " + demo.getName());
        demo.setName("multithreading");
        System.out.println("After changing the name of the thread is :\n " + demo.getName());
        System.out.println("The ID of the thread is :\n " + demo.getId());
        demo.start();
    }
}

