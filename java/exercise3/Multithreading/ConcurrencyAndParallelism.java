package com.java.training.core.Multithreading;

/*
Requirement:
    To Explain in words Concurrency and Parallelism

Entity:
    No

Function Declaration :
    No

Answer:
    Concurrency : when two tasks can start, run, and complete in overlapping time periods.
    It doesn't necessarily mean they'll ever both be running at the same instant. 
    Parallelism :  when tasks literally run at the same time.
 */
