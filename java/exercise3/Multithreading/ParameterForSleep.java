package com.java.training.core.Multithreading;

/*
Requirement:
     What are the arguments passed for the sleep() method

Entity :
    No

Function Declaration: 
    Sleep();
    
Answer:
    sleep(long milis)
    sleep(long milis, int nanos)
    The parameters are millisecond and nanoseconds
    millis: It defines the length of time to sleep in milliseconds  
    nanos: 0-999999 additional nanoseconds to sleep  

 */

