package com.java.training.core.LogicalOperator;
/*
Requirement:
    write a program to display the following using escape sequence in java
    A. My favorite book is "Twilight" by Stephanie Meyer
    B.She walks in beauty, like the night, Of cloudless climes and starry skies 
      And all that's best of dark and bright Meet in her aspect and her eyes�
    C."Escaping characters", � 2019 Java.

Entity : 
    EscapeCharatersDemo

Function Declaration :
    public static void main(String[] args);

Jobs to be Done :
    1. Using Escape Charters print the given statement 
       1.1) To include "" use \"
       1.2) To next line use \r\n
       1.3) To � use \u00A9 //� copy Rights 
    2. print the result
       
 */

public class EscapeCharatersDemo {
    
    public static void main(String[] args) {
        String escapeDemo = new String("A. My favorite book is \"Twilight\" by Stephanie Meyer");
        String escapeDemo1 = new String("B. She walks in beauty, like the night, \r\n" + 
                "Of cloudless climes and starry skies \r\n" + 
                "And all that's best of dark and bright \r\n" + 
                "Meet in her aspect and her eyes�\r\n" + 
                ""); 
        String escapeDemo2= new String("C. \"Escaping characters\", \u00A9 2019 Java."); 
        System.out.println(escapeDemo);
        System.out.println(escapeDemo1);
        System.out.println(escapeDemo2);
        
    }
}
