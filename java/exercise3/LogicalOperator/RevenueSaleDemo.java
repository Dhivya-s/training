/*
Requirement :
    Write a Java program to calculate the revenue from a sale based on 
    the unit price and quantity of a product input by the user.
    The discount rate is 10% for the quantity purchased between 100 and 120 units, 
    and 15% for the quantity purchased greater than 120 units.
    If the quantity purchased is less than 100 units, the discount rate is 0%.
    See the example output as shown below:

Entity:
    RevenueSaleDemo

Function Declaration :
    SalesDemo()
    public static void main(String[] args) 

Jobs to be Done :
    1.Get the inputs from the user
      1.1) Declare the class RevenueSaleDemo
    2.Invoke the method salesDemo from RevenueSaleDemo 
    3.Inside the method declare the variable unitPrice,
      Quantity,revenue,discountRate,discountAmount as float
      and declare the value as 0f.
    4.Get the quantity from user
      4.1) Check if the quality is between 100 and 120 
      4.2) If the condition is satisfied then fix the discountRate as 10 % (10/100);
      4.3) If condition Not satisfied then check if the quality is greater than 120
      4.4) If the condition satisfied then fix the discountRate as 15 % (15/100);
      4.5) If the condition not satisfied then check if the quality is less than 100
      4.6  If the condition satisfied then fix the discountRate as 0 % 
    5.Calculate revenue by multiplying quality with unitPrice
      5.1) calculate discountAmount by multiplying revenue with discountRate
      5.2) calculate After discount amount by subtracting discountAmount from revenue
      5.3) print the results
      
Pseudo code :

public class RevenueSaleDemo {
    
    public static void main(String[] args) {
        salesDemo();
    }

    private static void salesDemo() {
        float  unitPrice = 0f;
        float  quantity = 0f;
        float  revenue = 0f;
        float discountRate = 0f;
        float discountAmount = 0f;
        Scanner scanner//use scanner for input quantity and unitPrice
        if (quantity < 100) {
            revenue = quantity*unitPrice;
        } else if ((quantity <= 100) && (quantity >= 120)) {
            revenue = quantity*unitPrice;
            discountRate = 10/100;
            discountAmount =  revenue*discountRate;
            revenue -= discountAmount;
        } else if (quantity > 120) {
            revenue = quantity*unitPrice;
            discountRate = 15/100;
            discountAmount =  revenue*discountRate;
            revenue -= discountAmount;
        }
        System.out.println("The revenue from sale:  "+ revenue+"$");
        System.out.println("After discount :  "+discountAmount+"$"+discountRate*100+"%");
        
    }
}

 */
package com.java.training.core.LogicalOperator;

import java.util.Scanner;

public class RevenueSaleDemo {
    
    public static void main(String[] args) {
        salesDemo();
    }

    private static void salesDemo() {
        float  unitPrice = 0f;
        int  quantity = 0;
        float  revenue = 0f;
        float discountRate = 0f;
        float discountAmount = 0f;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter unitPrice : ");
        quantity = scanner.nextInt();
        System.out.println("Enter quantity : ");
        unitPrice = scanner.nextFloat();
        if (quantity < 100) {
            revenue = quantity*unitPrice;
        } else if ((quantity >= 100) && (quantity <= 120)) {
            revenue = quantity*unitPrice;
            discountRate = 10/100;
            discountAmount =  revenue*discountRate;
            revenue -= discountAmount;
        } else if (quantity > 120) {
            revenue = quantity*unitPrice;
            discountRate = 15/100;
            discountAmount =  revenue*discountRate;
            revenue -= discountAmount;
        }
        System.out.println("The revenue from sale:  "+ revenue+"$");
        System.out.println("After discount : "+discountAmount+"$("+discountRate*100+"%)");
        scanner.close();
    }
}

 