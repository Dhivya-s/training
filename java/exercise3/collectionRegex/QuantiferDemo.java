package com.java.training.core.collectionRegex;

/*
Requirement:
    write a program for java regex quantifier

Entity:
     QuantiferDemo 

Function Declaration:
    public static void main(String[] args)
    matches()

Jobs To be Done :
    1.Create a class as mentioned above.
    2.Under main method invoke the Pattern class and matches method from
      regex Pattern package
    3.Give the pattern and charSequence you want to match in print statement
    4.If it matches the sequence return true
    5.If it not matches the sequence return false
    
Pseudo code:

public class QuantiferDemo {
    
    public static void main(String args[]) {
        System.out.println(Pattern.matches("[dhivya]?", "a"));
        System.out.println(Pattern.matches("[dhivya]+", "aaa"));
        System.out.println(Pattern.matches("[dhivya]*", "dhii"));
        System.out.println(Pattern.matches("[a-zA-Z0-9]{6}", "Ayvi11"));  
        System.out.println(Pattern.matches("[789]{1}[0-9]{9}", "9953038949"));
    }
}
 */

import java.util.regex.Pattern;

public class QuantiferDemo {
    
    public static void main(String args[]) {
        System.out.println("? quantifier ....");
        System.out.println(Pattern.matches("[dhivya]?", "a"));
        
        //true (d or h or i or v or y or a comes one time)
        System.out.println("+ quantifier ....");
        System.out.println(Pattern.matches("[dhivya]+", "aaa"));
        
        //true ( d or h or i or v or y or a once or more times)
        System.out.println("* quantifier ....");
        System.out.println(Pattern.matches("[dhivya]*", "dhii"));
        
        //true ( d or h or i or v or y or a may come zero or more times)
        System.out.println("[] quantifier ....");
        System.out.println(Pattern.matches("[a-zA-Z0-9]{6}", "Ayvi11"));  
        
        //regular expression that accepts alphanumeric characters only.  
        //Its length must be six characters long only.
        System.out.println(Pattern.matches("[789]{1}[0-9]{9}", "9953038949"));//true  
        
        //regular expression that accepts 10 digit numeric characters 
        //starting with 7, 8 or 9 only.
    }
}
