package com.java.training.core.collectionRegex;
/*
Requirement:
    write a program for email-validation?

Entity:
    validMail

Function Declaration :
    public static boolean validMail(String id)
    public static void main(String[] args) 

Jobs To Be Done:
    1.Get mail Id From user
    2.Invoke the validMail method From the class EmailValidationDemo 
      3.1) Pass the user mailId as parameter
      3.2) Declare a mailId pattern 
      3.3) check if the user id is null then return false
      3.4) If the condition not satisfied 
      3.5) Check the mailId matches the pattern by invoking Matcher class
           from regex package
      3.6) If it matches returns True

pseudo code:

public class EmailValidationDemo {
    
    public static boolean validMail(String id) {
        String mailId = "^[a-zA-Z0-9_+&*-]+(?:\\."+  "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$"; 
        Pattern pattern //use pattern
        if (id == null) {
            return false;
        }
        Matcher matcher //use matcher
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner// use scanner
        System.out.println(validMail(mailId));
    }
}
 */


import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidationDemo {
    
    public static boolean validMail(String id) {
        String mailId = "^[a-zA-Z0-9_+&*-]+(?:\\."+  "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$"; 
     
        Pattern pattern = Pattern.compile(mailId);
        if (id == null) {
            return false;
        }
    
        Matcher matcher = pattern.matcher(id);
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the mailid ");
        String mailId = scanner.next();
        System.out.println(validMail(mailId));
        scanner.close();
    }
}
 
