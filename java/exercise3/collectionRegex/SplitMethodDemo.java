package com.java.training.core.collectionRegex;

import java.util.regex.Pattern;

/*
Requirement:
    For the following code use the split method() and print in sentence
    String website = "https-www-google-com";

Entity:
    SplitMethodDemo 

Function Declaration :
    splitDemo()
    public static void main(String[] args);

Jobs To be Done:
    1.Declare String REGEX as by which you are going to split the input
    2.Declare String INPUT as  "https-www-google-com"
    3.Invoke the Pattern class and pass the REGEX
    4.By using Pattern class object ivoke split method 
      4.1) Now split the input and store it in word array
      4.2) Using loop Print the splited inputs
    
Pseudo code:

public class SplitMethodDemo {
    
    String REGEX = "-";
    String INPUT = "https-www-google-com";
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile(REGEX);
        String[] words = pattern.split(INPUT);
        for (String word : words) {
            System.out.println(word);
        }
    }
 */

public class SplitMethodDemo {
    
    public static String REGEX = "-";
    public static String INPUT = "https-www-google-com";
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile(REGEX);
        String[] words = pattern.split(INPUT);
        System.out.println("After splited the input is");
        for (String word : words) {
            System.out.println(word);
        }
    }
}
