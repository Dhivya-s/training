/*
 Requirement :
   create a pattern for password which contains
   8 to 15 characters in length
   Must have at least one upper case letter
   Must have at least one lower case letter
   Must have at least one digit

Entity:
    PasswordDemo
Function Declaration:
    public static void main(String[] args)
    isupper()
    islower()
    isdigit()
    size()
Jobs to be done:
    1.Get input from the user and it mudt not be null 
    2.check the length is greater than 8
    3.check if it has at least one upper case letter
    4.check if it has at least one lower case letter
    5.check if it has at least one digit
    6.If all the conditions satisfied 
    7.print the password accepted
    8.if any one condition fails then 
    9.print password not accepted

pseudo code:
public class PasswordDemo {

    public static boolean ValidPassword(String password) {
        regex
        // using regex (0-9)(a-z)(A-Z){8,15}$

        Pattern pattern
        // use pattern
        if (password == null) {
            return false;
        }
        
        Matcher matcher
        \\use matcher
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner
        // use scanner to get input
        System.out.println("Enter the Password ");
        String password = sc.next();
        System.out.println(ValidPassword(password));
        sc.close();
    }
}
  
 */
package com.java.training.core.collectionRegex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordDemo {
    public static boolean validPassword(String password) {
        String word =  "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";
     
        Pattern pattern = Pattern.compile(word);
        if (password == null) {
            return false;
        }
    
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Password ");
        String password = scanner.next();
        System.out.println(validPassword(password));
        scanner.close();
    }
}
 