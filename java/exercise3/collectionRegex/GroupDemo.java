package com.java.training.core.collectionRegex;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GroupDemo {
    
     public static void main(String[] args) { 
         
         String regex = "(all)"; 
         // Create a pattern from regex 
         Pattern pattern = Pattern.compile(regex); 
         // Get the String to be matched 
         String stringToBeMatched  = "fireball"; 
         // Create a matcher for the input String 
         Matcher matcher  = pattern.matcher(stringToBeMatched); 
         // Get the current matcher state 
         MatchResult result = matcher.toMatchResult(); 
         System.out.println("Current Matcher: " + result); 
         while (matcher.find()) { 
             // Get the group matched using group() method 
             System.out.println(matcher.group()); 
        } 
    } 
} 


