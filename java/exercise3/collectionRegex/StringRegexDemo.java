package com.java.training.core.collectionRegex;

/*
Requirement:
    To write a program for Java String Regex Methods?
 
Entity:
    StringRegexDemo
    
Function declaration:
    public static void main (String[] args);
    matches();

Jobs To be done:
   1. Create a class as mentioned above 
   2. Under the main method invoke the Pattern class From Regex
      2.1) Give the pattern which you want to match
      2.2) Invoke the Matcher class from Regex 
      2.3) Using matcher method it check if the matcher matches it returns true
      2.4) iF it not matches it return false
  
Pseudo code :

public class StringRegexDemo {

     public static void main(String[] args) {
        System.out.println(Pattern.matches("..s","vas"));
    }
}

 */
import java.util.regex.*;

public class StringRegexDemo {
    
    public static void main(String[] args) {
        System.out.println(Pattern.matches("..s","vas"));
        
        //..s checks the third character is s or not
    }
}
