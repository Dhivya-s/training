/*
Requirement:
    demonstrate establish the difference between lookingAt() and matches().

Entity:
    RegexLookingAtMatchesDemo

Function Declaration:
    lookingAt()
    matches()
    
Jobs to be done :
    1.Declare two strings name as word and words
      and assign values for both 
    2.By using lookingAt() returns true
      if, and only if, a prefix of the input sequence matches this matcher's pattern.
    3.Matches() This method returns true if, and only if,
      this string matches the given regular expression.
    4.Print the boolean values

pseudo code:

public class RegexLookingAtMatchesDemo {
    
    public static String word = "fire";
    public static String words = "fire ball";
    public static void main(String[] args) {
        pattern pattern
        Matcher matcher
        System.out.println("lookingAt(): "+ matcher.lookingAt());
        System.out.println("matches(): "+ matcher.matches());
        
    }

}

 */

package com.java.training.core.collectionRegex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexLookingAtMatchesDemo {
    
    public static String word = "fire";
    public static String words = "fireball";
    public static Pattern pattern;
    public static Matcher matcher;
    public static void main(String[] args) {
        pattern = Pattern.compile(word);
        matcher = pattern.matcher(words);
        System.out.println("lookingAt(): "+ matcher.lookingAt());
        System.out.println("matches(): "+ matcher.matches());
    }

}
