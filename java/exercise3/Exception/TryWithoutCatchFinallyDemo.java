package com.java.training.core.Exception;


/*
 Requirement:
    only try block without catch and finally blocks
    
Entity:
    TryWithoutCatchFinallyDemo
    
Function declaration:
    public static void main(String[] args)
    
 Jobs to be done:
    In a a program try block should be accompanied by either catch block or finally block or both.
    A program without catch and finally block will throw syntax error as the exception will not get handled.

*/


public class TryWithoutCatchFinallyDemo {
    
    public static void main(String[] args) {
        /*try{
            System.out.println(1/0);
        }*/
       //Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
       //Syntax error, insert "Finally" to complete BlockStatements
    }
    
}