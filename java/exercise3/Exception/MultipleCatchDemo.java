package com.java.training.core.Exception;

/*
Requirement:
    Requirement:*catching multiple exception with example. 
    Is it possible to have more than one try block?
 

Entity:
    public class MultipleCatchDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.Exception.
    2. Declare the class MultipleCatchDemo as public.
    3. Under a main method declare a try block.
    4. The try block contains the initialization of an integer array named numbers and declare values.as 1,2,3.
    5. Now call the value not in array .
    6. Then print the number[10] .
    7. Now give multiple exception block stating Arithmetic exception ArrayIndexOutOfBoundsException.
    8. Finally give the print statement "the program executed"  
*/

/*   Reason:No each Try block must be handled using catch block
     If you use multiple try block for many catch it will generate an error during compile tile

 */

public class MultipleCatchDemo {
    
    public static void main(String[] args) {
        try {    
            int[] numbers = {1, 2, 3};
            System.out.println(numbers[10]); 
        } catch (ArithmeticException e) { 
              System.out.println("Arithmetic Exception occurs");  
        } catch(ArrayIndexOutOfBoundsException e) { 
              System.out.println("ArrayIndexOutOfBounds Exception occurs");  
        } catch(Exception e) {
              System.out.println("Parent Exception occurs");  
        }             
           
        System.out.println("The program executed");    
    }
}
