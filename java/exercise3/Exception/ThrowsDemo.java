package com.java.training.core.Exception;
/*
Requirement:
    To demonstrate the difference between throw and throws.

Entity:
    public class ThrowsDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.Exception.
    2. Declare the class ThrowsDemo as public.
    3. Create a method division() to demonstrate throws clause.
    4. print the results. 
    
pseudo code:

public class ThrowsDemo {
    public int division(int number1, int number2) throws ArithmeticException {
        int value = number1/number2;
        return value;
    }
    public static void main(String args[]) {
        Throws // use throws 
        try{
            System.out.println(demo.division(15,0));
        }
        catch(ArithmeticException e) {
            System.out.println("You shouldn't divide number by zero");
        }
    }
}
*/

public class ThrowsDemo {
    public int division(int number1, int number2) throws ArithmeticException {
        int value = number1/number2;
        return value;
    }
    public static void main(String args[]) {
        ThrowsDemo demo = new ThrowsDemo();
        try{
            System.out.println(demo.division(15,0));
        }
        catch(ArithmeticException e) {
            System.out.println("You shouldn't divide number by zero");
        }
    }
}