package com.java.training.core.Exception;

/*
Requirement:
    To demonstrate the difference between Checked and Unchecked Exception.

Entity:
    public class CheckedAndUncheckedDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.Exception.
    2. Import the package FileInputStream.
    3. Now compare checked with unchecked.
    4. The exception thrown at the compile time is called Checked Exception.
    5. The exception thrown at the run time is called unChecked Exception.
    
pseudo code
public class CheckedUncheckedDemo { 
    
    public static void main(String args[]) throws IOException {
        
        //unchecked example 
        
        try {
            int arr[] ={1,2,3,4,5};
            System.out.println(arr[7]);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("The specified index does not exist in array. Please correct the error.");
        }
        Use FileInputStream //cheched example
*/

import java.io.FileInputStream;
import java.io.IOException;

public class CheckedUncheckedDemo { 
    
    public static void main(String args[]) throws IOException {
        
        //unchecked example 
        
        try {
            int arr[] ={1,2,3,4,5};
            System.out.println(arr[7]);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("The specified index does not exist in array. Please correct the error.");
        }
        
        //checked example
        // Checked Exception - Throws compile time exception.
        //It throws FileNotFoundException if the file is not found
        FileInputStream file = null;
        file = new FileInputStream("C:/myfile.txt");
        int text;
        while ((text = file.read()) != -1) {
            System.out.println((char)text);
        }
        
        file.close(); // Throws IO exception.*
    }
}