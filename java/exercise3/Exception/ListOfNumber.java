package com.java.training.core.Exception;

/*
Requirement:
    To write a program using try and catch.

Entity:
    public class ListOfNumber.

Function Declaration:
    public static void main(String[] args)
    public voidwriteList()

Jobs to be done:
    1. Create a package com.java.training.core.Exception.
    2. Declare the class ListOfNumber as public.
    3. Create an integer array arrayOfNumbers of size 10.
    4. Declare the method public void writeList()
    5. The method body contains try and catch block of NumberFormatException and
       IndexOutOfBounds exception.
    6. Under a main method create an object for the class ListOfNumber as list.
    7. Now call the writeList() method and print the result.
    
pseudo code:

public class ListOfNumber {
    
    int[] array //use array
    
    public void writeList() {
        try {
            arrayOfNumbers[10] = 11;
        } catch (NumberFormatException e1) {
            System.out.println("NumberFormatException:" + " " + e1.getMessage());
        } catch (IndexOutOfBoundsException e2) {
            System.out.println("IndexOutOfBoundsException:" + " " + e2.getMessage());
        }
    }
        
    public static void main(String[] args) {
        ListOfNumber list// use list
        list.writeList();
    }
}
*/

public class ListOfNumber {
    
    public int[] arrayOfNumbers = new int[10];
    
    public void writeList() {
        try {
            arrayOfNumbers[10] = 11;
        } catch (NumberFormatException e1) {
            System.out.println("NumberFormatException:" + " " + e1.getMessage());
        } catch (IndexOutOfBoundsException e2) {
            System.out.println("IndexOutOfBoundsException:" + " " + e2.getMessage());
        }
    }
        
    public static void main(String[] args) {
        ListOfNumber list = new ListOfNumber();
        list.writeList();
    }
}

