/*
Requirement:
    To demonstrate the difference between throw and throws.

Entity:
    public class ThrowDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.Exception.
    2. Declare the class ThrowDiffernceDemo as public.
    3. create a method checkAge() to demonstrate the example for throw.
    4. print the results. 
    
pseudo code:

public class ThrowDemo {
    public void checkAge(int age) {
        if(age < 18)
            throw //use throw
        else
            System.out.println("Eligible for voting");
        }
        public static void main(String args[]) {
            ThrowDemo demo = new ThrowDemo();
            demo.checkAge(13);
           System.out.println("End Of Program");
        }
}


*/
package com.java.training.core.Exception;

public class ThrowDemo {
    public void checkAge(int age) {
        if(age < 18)
            throw new ArithmeticException("Not Eligible for voting");
        else
            System.out.println("Eligible for voting");
        }
        public static void main(String args[]) {
            ThrowDemo demo = new ThrowDemo();
            demo.checkAge(13);
           System.out.println("End Of Program");
        }
}

