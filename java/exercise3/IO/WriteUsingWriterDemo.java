/*
Requirement:
    To Write some String content using Writer

Entity:
    WriteUsingWriterDemo 

Function Declaration :
    public static void main(String[] args)

Jobs To be Done :
    1.Declare the class as mentioned below 
    2.Under the main method declare variable as content and type as string 
      2.1) Declare String and store in content
      2.2) Inside try block invoke the Writer and declare the file path
      2.3) Get the string and Using Writer's object invoke write method and pass parameter content
      2.4) If any error exception caused the catch block catch the exception and print it.

Pseudo code:
public class WriteUsingWriterDemo {
    
    public static void main(String[] args) {
        try {
            Writer writer //use writer 
            String content = "Welcome to java learning";
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            System.out.print(e);
        }
        System.out.println("Successfully executed");
    }
}

 */
package com.java.training.core.IO;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriteUsingWriterDemo {
    
    public static void main(String[] args) {
        try {
            Writer writer = new FileWriter("D:\\textfile.txt");
            String content = "Welcome to java learning";
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            System.out.print(e);
        }
        System.out.println("successfully executed");
    }
}
