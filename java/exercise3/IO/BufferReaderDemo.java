/*
Requirement:
    Read a any text file using BufferedReader and print the content of the file

Entity:
    BufferReaderDemo 

Function Declaration :
    public static void main(String[] args)

Jobs to Be Done:
    1. Declare the class as Mentioned below
    2. Under main method use throws block and  invoke the FileReader Class and pass the Directory
       2.1) invoke the BufferedReader Class and pass the object of  FileReader
    3. Declare the variable content and read the first character 
       3.1) Check the character is not equal to -1
       3.2) If the condition satisfied print the character
       3.3) now read the next character and store it content
       3.4) The loop continue till the condition not satisfies
    4.If any exception come it throws error 

Pseudo code:

public class BufferReaderDemo {
    
    public void main(String[] args)throws Exception {
        FileReader reader1 // use FileReader ("D:\\\\textfile.txt");
        BufferedReader reader2 // use  BufferedReader
        int content ;
        while ((content = reader2.read())!=-1) {
            System.out.print((char)content);
        }
        reader2.close();
        reader1.close();
    }
}
 */
package com.java.training.core.IO;

import java.io.BufferedReader;
import java.io.FileReader;

public class BufferReaderDemo {
    
    public void main(String[] args)throws Exception {
        FileReader reader1 = new FileReader("D:\\textfile.txt");
        BufferedReader reader2 = new BufferedReader(reader1);
        int content ;
        while ((content = reader2.read())!=-1) {
            System.out.print((char)content);
        }
        reader2.close();
        reader1.close();
    }

}
