/*
Requirement:
    InputStream to String and vice versa

Entity:
    InputStreamToString

Function Declaration:
     public static void main(String[] args)

Jobs To Be Done:
    1.Create InputStream Class and pass the parameter as "text" and get it as bytes
    2.Create BufferReader class and pass the parameter using InputStreamReader(stream)
    3.Using loop read the text and read using readline() and check it is not null and 
      store it in line type string
    4.Create StringBuilder class and Append the line in StringBuilder's object builder
      and print the result

Pseudo code:

public class  InputStreamToString {

    public static void main(String[] args) throws IOException {

        InputStream stream = new ByteArrayInputStream("Hello there!".getBytes());
        StringBuilder builder //create StringBuilder
        String line;

        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        reader.close();

        System.out.println(builder);

    }
}
    
 */
package com.java.training.core.IO;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class  InputStreamToString {

    public static void main(String[] args) throws IOException {

        InputStream stream = new ByteArrayInputStream("Hello everyone !".getBytes());
        StringBuilder builder = new StringBuilder();
        String line;

        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        reader.close();

        System.out.println(builder);

    }
}
