/*
Requirement :
    Write some String content using OutputStream

Entity:
    WriteUsingOutputStream

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done :
    1.Declare the class as mentioned below 
    2.Under the main method declare variable as content and type as string 
      2.1) Declare String and store in content
      2.2) Inside try block invoke the OutputStream and declare the file path
      2.3) Get the string and convert to byte and store it in bytes
      2.4) Using OutputStream's object invoke write method and pass parameter which you store in bytes
      2.5) If any error exception caused the catch block catch the exception and print it

Pseudo code :

public class WriteUsingOutputStream {
    
    public static void main(String[] args) {
        String content = "This is a line of text inside the file.";
        try {
            OutputStream stream // use OutputStream ("D:\\textfile.txt");
            byte[] bytes = content.getBytes();
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            System.out.print(e);
        }
    }
}

 */

package com.java.training.core.IO;

import java.io.FileOutputStream;
import java.io.OutputStream;

public class WriteUsingOutputStream {
    
    public static void main(String[] args) {
        String content = "This is a line of text inside the file.";
        try {
            OutputStream stream = new FileOutputStream("D:\\textfile.txt");
            byte[] bytes = content.getBytes();
            stream.write(bytes);
            System.out.print("content is written to the file");
            stream.close();
        } catch (Exception e) {
            System.out.print(e);
        }
    }
}
