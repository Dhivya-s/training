/*
Requirement:
     Reading a file using InputStream

Entity: 
     ReadUsingInputStream

Function Declaration:
    public static void main(String[] args)

Jobs To be Done :
    1.Declare the class as mentioned above
    2.Inside Try Invoke the FileInputStream class and Pass The file destination
    3.Declare the variable content and initialize the value as Zero
     3.1) Read the textfile.txt from initialized character to last character
          using while loop . the loop ends when it reaches to -1.
     3.2) print the readed characters 
    4. If any exception caused the catch method catches the error and Print it
     
Pseudo code:
public class ReadUsingInputStream {

    public static void main (String[] args) {
        try {
            FileInputStream stream //use file Stream input ("D:\\textfile.txt");
            int content = 0;
            while ((content = stream.read())!=-1) {
                System.out.print((char)content);
            }
            stream.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }
}
    
 */
package com.java.training.core.IO;

import java.io.FileInputStream;

public class ReadUsingInputStream {
    
    public static void main (String[] args) {
        try {
            FileInputStream stream = new FileInputStream("D:\\textfile.txt");
            int content = 0;
            while ((content = stream.read())!=-1) {
                System.out.print((char)content);
            }
            stream.close();
        } catch (Exception e){
            System.out.println(e);
        }
            
            
    }
}

