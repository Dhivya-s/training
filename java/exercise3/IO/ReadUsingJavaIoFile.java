/*
Requirement:
    To Read a file using java.io.File.

Entity:
    ReadUsingJavaIoFile 

Function Declaration :
    public static void main(String[] args

Jobs To Be Done:
    1.Declare a class as mentioned above 
    2.Under a main method inside a try a block invoke aFile class
      2.1) And pass the path of the file 
      2.2) invoke a method canRead from File class 
      2.3) Store the boolean value in content
      2.4) check the value is equal to true 
      2.5) If the condition satisfied print the statement
      2.6) If the condition not satisfied else statement will be printed
   3. If any error exception caused the catch block catch the exception and print it.

Pseudo code:

public class ReadUsingJavaIoFile {

    public static void main(String[] args) {
        try {
            File file // use file
            boolean content = file.canRead();
            if (content == true) {
                System.out.print("Can Read the file : " + content);
            } else {
                System.out.println("can't read check the path");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
 */

package com.java.training.core.IO;

import java.io.File;

public class ReadUsingJavaIoFile {
    
    public static void main(String[] args) {
        try {
            File file = new File("D:\\textfile.txt");
            boolean content = file.canRead();
            if (content == true) {
                System.out.print("Can Read the file : " + content);
            } else {
                System.out.println("can't read check the path");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
