package com.java.training.core.IO;

/*
Requirements : 
    Get the file names of all file with specific extension in a directory.
 
Entities :
    FileName

Method Signature :
    public static void main(String[] args)

Jobs To Be Done:
    1)Create a object for File with directory as argument.
    2)Add all file names to the array.
    3)for each file name in the array.
      3.1) check whether the extension matches with given extension.
      3.2) if matches, Print the file names.
 
PseudoCode:
 
class FileExtensionDemo {
 
    public static void main(String[] args) {
        File file = new File("C:\\User\\asus\\eclipse-workspace\\javaee-demo");
        File[] fileArray = file.listFiles();

        for (File files : fileArray ) {
            //check and print the file name which matches the given extension.
        }           
    }
 }
 */

import java.io.File;

public class FileExtensionDemo {

    public static void main(String[] args) {
        File file = new File("C:\\User\\asus\\eclipse-workspace\\javaee-demo");
        File[] fileArray = file.listFiles();
        
        for (File files : fileArray ) {
            if(files.getName().toLowerCase().endsWith(".txt")) {
                System.out.println(files.getName());
            }
        }
        
    }
}