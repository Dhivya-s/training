/*
Requirement:
    To  Reading a file using Reader

Entity:
    ReadUsingReaderDemo

Function Declaration :
    public static void main (String[] args)

Jobs To Be Done:
    1. Declare the class as Mentioned below
    2. Under main method use try block and  invoke the Reader Class and pass the Directory
    3. Declare the variable content and read the first character 
       3.1) Check the character is not equal to -1
       3.2) If the condition satisfied print the character
       3.3) now read the next character and store it content
       3.4) The loop continue till the condition not satisfies
    4.If any exception come catch block catches the error and print it

Pseudo code:

public class ReadUsingReaderDemo {
    
    public static void main (String[] args) {
        try {
            Reader reader//use reader class ("D:\\textfile.txt");
            int content = reader.read();
            while (content !=-1) {
                System.out.print((char)content);
                content = reader.read(); 
            }
            reader.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }
}
 */
package com.java.training.core.IO;

import java.io.FileReader;
import java.io.Reader;

public class ReadUsingReaderDemo {
    
    public static void main (String[] args) {
        try {
            Reader reader = new FileReader("D:\\textfile.txt");
            int content = reader.read();
            while (content !=-1) {
                System.out.print((char)content);
                content = reader.read(); 
            }
            reader.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }
}
