package com.java.training.core.IO.Stream;
/*
Requirement:
    program to write primitive dataTypes to file using by dataOutputstream.
  
Entity :
    PrimitiveDataTypesDemo
  
Function Declaration :
    public static void main(String[] args);
  
Jobs to be done: 
    1. Create  the FileOutputStream and create object and pass destination file for writing.
    2. Create the DatOutputStream using declared FileOutputStream.
    3. Write the primitive data types to file.
    4. Close the DataOutputStream.
    
PseudoCode:

public class PrimitiveDataTypesDemo {
     
    public static void main(String[] args) throws IOException {
               
        FileOutputStream fileOutputStream = new FileOutputStream("D:\\text.txt");
        DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
        dataOutputStream.writeInt(30000);
        dataOutputStream.writeShort(1500);
        dataOutputStream.writeByte(100);
        dataOutputStream.close();     
    }
}

 */
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class PrimitiveDataTypesDemo {
     
    public static void main(String[] args) throws IOException {
               
        FileOutputStream fileOutputStream = new FileOutputStream("D:\\text.txt");
        DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);
        dataOutputStream.writeInt(30000);
        dataOutputStream.writeShort(1500);
        dataOutputStream.writeByte(100);
        dataOutputStream.close();     
    }
}

