package com.java.training.core.IO.Stream;

/*
Requirement: 
    To read the file contents  line by line in streams with example.
 
Entity:
    ReadFileDemo
 
Function Declaration:
    public static void main(String[] args);
 
Jobs to be done: 
    1.Create instance of path and stored in variable filePath 
    2.Invoke the Stream class and read the file 
    3.Using forEach in stream and print it 
                
PseudoCode:
 
public class ReadFileDemo {
    
    public static void main(String[] args) throws IOException {
        
        Path filePath = Paths.get("D:\text.txt");
        Stream<String> stream = Files.lines(filePath) ;
        stream.forEach(System.out::println);        
    }
}

 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadFileDemo {
    
    public static void main(String[] args) throws IOException {
        
        Path filePath = Paths.get("D:\\text.txt");
        Stream<String> stream = Files.lines(filePath) ;
        stream.forEach(System.out::println);        
    }
}

