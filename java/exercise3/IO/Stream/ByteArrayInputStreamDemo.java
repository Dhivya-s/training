package com.java.training.core.IO.Stream;
    
/*
Requirement: 
    Write a program for ByteArrayInputStream class to read byte array as input stream.
  
Entity: 
    ByteArrayAsInputStream
  
Function Declaration:
    public static void main(String[] args);
    
Jobs to be done: 
    1.Create a byte array named byteArray and store values to it.
    2.Create a ByteArrayInputStream by passing byteArray as argument.
    3.While the byte array is not empty
      3.1) Convert the int to char 
      3.2) Print the buffer element and the character.

PseudoCode:
 
public class FileReaderAndWritterDemo {
    
    public static void main(String[] args) throws IOException {  
        byte[] byteArray = { 11, 12, 13, 14 };  
        
        // Create the new byte array input stream  
        ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);  
        int value ;  
        while ((value = byteStream.read()) != -1) {   
            
            //Conversion of a byte into character
            char character = (char) value;  
            System.out.println("ASCII value of Character is:" + value + "; Special character is: " + character);  
        }  
    }  
}
 */
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ByteArrayInputStreamDemo {
    
    public static void main(String[] args) throws IOException {  
        byte[] byteArray = { 11, 12, 13, 14 };  
        
        // Create the new byte array input stream  
        ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);  
        int value ;  
        while ((value = byteStream.read()) != -1) {   
            
            //Conversion of a byte into character
            char character = (char) value;  
            System.out.println("ASCII value of Character is:" + value + "; Special character is: " + character);  
        }  
    }  
}
