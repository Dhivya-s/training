package com.java.training.core.IO;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/*
Requirement:
    To convert String To InputStream

Entity:
    StringToInputStreamDemo 

Function Declaration:
    public static void main(String args[]) 
    
Jobs To be done:
    1.Declare The variable name as name and type String 
    2.Inside try Create InputStream and pass the name and get it as bytes
      2.1) Print the stream
    3.Using available method Print the available bytes 
      using read method read the stream and print it.
    4.If any error come catch block catches the error and print it 

Pseudo code:

public class StringToInputStreamDemo {
    
    public static void main(String args[]) {

        // Creates a string
        String name = "hello word";
        System.out.println("String is: " + name);

        try {

          InputStream stream // use InputStream stream 
          System.out.println("InputStream: " + stream);
          stream.close();
        }

        catch (Exception e) {
          System.out.println(e);
        }
    }
}

*/
public class StringToInputStreamDemo {
    
    public static void main(String args[]) {

        // Creates a string
        String name = "hello word";
        System.out.println("String is: " + name);
        try {

            InputStream stream = new ByteArrayInputStream(name.getBytes());
            System.out.println("InputStream: " + stream);

            // Returns the available number of bytes
            System.out.println("Available bytes at the beginning: " + stream.available());

            // Reads 3 bytes from the stream stream
            stream.read();
            stream.read();
            stream.read();

            // After reading 3 bytes
            // Returns the available number of bytes
            System.out.println("Available bytes at the end: " + stream.available());
            stream.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
