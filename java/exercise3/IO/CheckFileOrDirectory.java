package com.java.training.core.IO;


/*
Requirement:
    Given a path, check if path is file or directory

Entity:
    CheckFileOrDirectory

Function Declaration :
    public static void main(String[] args) 

Jobs To Be Done:
    1.Invoke the File class and pass the parameter as path of the file
    2.Using dir object pass the directory to the File class 
      2.1) Using  isFile method If it is a file it will return true  else false
      2.2) Using isDirectory method ,if it is a directory it return true else false
      2.3) And print the result.

Pseudo code:

public class CheckFileOrDirectory {
    
    public static void main(String[] args) {
        File file // use file and give the path
        File dir // use file and give the directory
        System.out.println("D:\\textfile.txt ? "+file.isFile());
        System.out.println("D:\\ ?"+dir.isDirectory());
    }

}

 */

import java.io.File;

public class CheckFileOrDirectory {
    
    public static void main(String[] args) {
        File file = new File("D:\\textfile.txt");
        File dir = new File("D:\\");
        System.out.println("D:\\textfile.txt ? "+file.isFile());
        System.out.println("D:\\ ?"+dir.isDirectory());
    }

}
