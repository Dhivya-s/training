/*
Requirement :

  1. To Create a list
  2. Add 10 values in the list
  3. Create another list and perform addAll() method with it
  4. Find the index of some value with indexOf() and lastIndexOf()
  5. Print the values in the list using For loop, For Each,Iterator,Stream API 
  6. Convert the list to a set
  7. Convert the list to a array
    
Entity :

    ListDemo
    
Function Declaration :

    addAll(),
    indexOf(),
    lastIndexOf(),
    public static void main (String[] args)  
    
Jobs to be done :
  1. create a package com.java.training.core.list
  2. Declare class as ListDemo and create object as list
  3. using that object add 10 values to the list
  4. create another list called list1 and perform addAll() method with it
  5. use the index of some value with indexOf() and lastIndexOf()
  6. Then Print the values in the list using For loop, For Each,Iterator,Stream API 
  7. Then Convert the list to a set by creating set 
  8. Convert the list to a array by creating array and by using toArray() method
    
pseudo code :

public class ListDemo {
    
    public static void main(String[] args) {
    
        //creating a list
         List list 
         
        //Adding vales in the list
        list.add(19);
        System.out.println(" The list values are" + list);
        List list1 
        
        // creating new list
        list1.add(9);
        System.out.println("The values are  " +  list1);
        
        //using addAll()
        list.addAll(list1);
        
        // print using for loop
        
        for (int i = 0; i < list1.size(); i++) {
            System.out.println("The list elements are  : " + list1);
        }
        
        // print using for each
        
        for (Integer value : list1 ) {
            System.out.println(" The values are " + value);
        }
        
        // print using iterator
        
        Iterator iteratorList //use iterator
        while(iteratorList.hasNext()) {
            System.out.println("The list values are  " + iteratorList.next());
        }
        
        // print using Stream API 
        
        list1.stream()
           
        // to find first index 
        
        System.out.println(list1.indexOf(8));
           
        // to find last index
           
        System.out.println( list1. lastIndexOf(19));
        
        // Creating an array for a list
       
        Integer[] firstArray //use array
        firstArray= list.toArray(firstArray);
        System.out.println("the list in array "+ " " + Arrays.toString(firstArray));
        
        // Converting List into an Set
        
        Set number//use set
        System.out.println("The Set Values are" );
        for ( Integer value1 :  numberSet) {
            System.out.println(value1);
        }
    }

}

 */

package com.java.training.core.list;

import java.util.*;

public class ListDemo {
    
    public static void main(String[] args) {
        //creating a list
        
        List<Integer> list = new ArrayList<>();
        //Adding vales in the list
        
        list.add(10);
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(16);
        list.add(17);
        list.add(18);
        list.add(19);
        System.out.println(" The list values are" + list);
        
        List<Integer> list1 = new ArrayList<>();
        
        // creating new list
        
        list1.add(8);
        list1.add(9);
        System.out.println("The values are  " +  list1);
        
        //using addAll()
        
        list.addAll(list1);
        
        // print using for loop
        
        for (int i = 0; i < list1.size(); i++) {
            System.out.println("The list elements are  : " + list1);
        }
        
        // print using for each
        
        for (Integer value : list1 ) {
            System.out.println(" The values are " + value);
        }
        
        // print using iterator
        
        Iterator<Integer> iteratorList = list1.iterator();
        while(iteratorList.hasNext()) {
            System.out.println("The list values are  " + iteratorList.next());
        }
        
        // print using Stream API 
        
        list1.stream().forEach ((element) -> System.out.println(element));
           
        // to find first index 
        
        System.out.println(" The first index is " + list1.indexOf(8));
           
        // to find last index
           
        System.out.println(" The last index is " + list1. lastIndexOf(19));
        
        // Creating an array for a list
       
        Integer[] firstArray = new  Integer[list.size()];
        firstArray= list.toArray(firstArray);
        System.out.println("the list in array "+ " " + Arrays.toString(firstArray));
        
        // Converting List into an Set
        
        Set< Integer> numberSet = new HashSet<>(list);
        System.out.println("The Set Values are" );
        for ( Integer value1 :  numberSet) {
            System.out.println(value1);
        }
    }

}


