/*
Requirement:
    1.Explain about contains(), subList(), retainAll() and give example

Entity:
    ListDemo1

Function declaration:
    public static void main(String[] args),
    contains(),
    subList(), 
    retainAll().

Jobs to be done :
    1.Create a class ListDemo1 as public 
    2.Declare a main method and Create a new list
    3.Add some values
    4.use contains(), subList(), retainAll()
    5.print values
*/
package com.java.training.core.list;

import java.util.*;
public class ListDemo1 {
    public static void main(String[] args){
        List<String> list = new ArrayList<>();
        list.add("Dhivya sankaran");
        list.add("Maheswari sankaran");
        list.add("latha");
        list.add("sankaran");
        
        //contains() check the value is present or not 
        
        if (list.contains("latha") == true) {
            System.out.println(" the value is present ");
        } else {
            System.out.println("the value is not present");
        }
        
        //sublist 
        System.out.println(list.subList(1,3));
        
        //retainAll()
        List <String> numberList = new ArrayList<>();
        numberList.add("1");
        numberList.add("2");
        System.out.println("The values are " + numberList);
        System.out.println(numberList.retainAll(list));
        System.out.println( "the changed values are " + numberList);
    }
}


