package com.java.training.core.Generic;

/*
Requirement:
    To  Write a program to print employees name list by implementing iterable interface.

Entity:
    class Employee.
    class EmployeeList.
    public class IterableInterfaceDemo.

Function Declaration:
    public String getName()
    public void setName(String Name)
    public void addEmployee(Employee employee)
    public void removeEmployee(Employee employee)
    public static void main(String[] args)
    public Iterator<Employee> iterator()
    
Jobs to be done:
   
    1. Create a constructor and read the values.
    2. Using getters and setters get and set Name,
    3. Now create a class EmployeeList of type Employee by implementing from Iterable.
    4. Create a constructor and store the values as list.
    5. Now add and remove the employee using addEmployee() method removeEmployee method().
    6. Create a class IterableInterfaceDemo as public.
    7. Under a main method Create an object for Employee as employee1 and employee2.
    8. Create an object for EmployeeList and add the values from employee1 and employee2.
    9. Using for each loop print the list. 
    
pseudo code:

class Employee {
    public Employee(String name) {
        this.name = name;
        
    } 
    getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
    

class EmployeeList implements Iterable<Employee> {
    private List<Employee> employees;
    Array list//use array list
    }
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }
    
    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }    
    
}

public class IterableInterfaceDemo {
    public static void main(String[] args) {
        Employee employee1 = new Employee("John");
        Employee employee2 = new Employee("Joseph");
        EmployeeList employeeList //list
        employeeList.addEmployee(employee1);
        employeeList.addEmployee(employee2);
        for (Employee employee : employeeList) {
            System.out.println(employee);
        }
    }
}


*/
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

class Employee {

    private String name;
   

    public Employee(String name) {
        this.name = name;
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
    

class EmployeeList implements Iterable<Employee> {
    private List<Employee> employees;
    
    public EmployeeList() {
        employees = new ArrayList<Employee>();
    }
    
   
    
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }
    
    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }



    @Override
    public Iterator<Employee> iterator() {
        // TODO Auto-generated method stub
        return null;
    }
    
    
}

public class IterableInterfaceDemo {
    public static void main(String[] args) {
        Employee employee1 = new Employee("John");
        Employee employee2 = new Employee("Joseph");
        EmployeeList employeeList = new EmployeeList();
        employeeList.addEmployee(employee1);
        employeeList.addEmployee(employee2);
        for (Employee employee : employeeList) {
            System.out.println(employee);
        }
    }
}
