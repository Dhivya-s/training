package com.java.training.core.StreamTokenizer;

/*
Requirement:
    What is stream tokenizer?
Solution:
    The Java. io. StreamTokenizer class takes an input stream and parses it into "tokens",
    allowing the tokens to be read one at a time. The stream tokenizer can recognize identifiers,
    umbers, quoted strings,and various comment styles.
 */
