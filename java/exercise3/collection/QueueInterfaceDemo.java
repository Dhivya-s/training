/*Requirement:
    The difference between poll() and remove() method of queue interface

 Entity:
    PopRemoveDemo
 
 Function Declaration:
   public static void main(String[] args){}
 
 Jobs To Be Done:
  1)create a linkedList queue .
  2)use poll method 
  3)Inside the try block use remove method
  3.1)if queue is empty the exception will be handled
  3.2)Otherwise removed element will be printed
  
pseudo code:

public class PopRemoveDemo {

    public static void main(String[] args) {

       Queue<String> queue = new LinkedList<>();
       System.out.pritln(queue.poll());
       try {
           System.out.println(queue.remove());
       } catch(Exception exception) {
           exception will be handled
       }
    }   
}

Difference:Both poll() and remove() removes the first element in the queue.
    If the queue is empty,
    poll() will return null
    remove() will throw NoSuchElementException
*/
package com.java.training.core.collection;

import java.util.LinkedList;
import java.util.Queue;

public class QueueInterfaceDemo {

    public static void main(String[] args) {

        Queue<String> queue = new LinkedList<>();
        System.out.println("polled Elements are :" + queue.poll());
        try {
            System.out.println("Removed Element:" +queue.remove());
        } catch(Exception exception) {
            System.out.println("Throws NoSuchElementException");
        }
    }   
}
