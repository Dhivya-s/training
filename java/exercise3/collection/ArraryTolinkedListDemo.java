/*Requirement:
 create an array list with 7 elements, and create an empty linked list add all elements of the
 array list to linked list ,traverse the elements and display the result .
  
Entity:
    ArrayToLinkedListDemo

Function Declaration:
    public static void main(String[] args)
 
Jobs To Be Done: 
    1.create a array list.
    2.Add the elements to the array list.
    3.print the array list.
    4.create a linked list.
    5.add the elements of the array list to the linked list.
    6.Iterate the liked list till the iteration reached the length of the linked list.
    7.print the elements of the liked list.

pseudo code:
    public class ArrayToLinkedList {

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        
        Add elements to arrayList
        System.out.println("Array list elements are " + arrayList);
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        Add all elements of arrayList to linkedList
        System.out.println("Elements of the linked list:");
        for (int number : linkedList) {
            System.out.println(number);
        }

    }
 */
package com.java.training.core.collection;


import java.util.ArrayList;
import java.util.LinkedList;

public class ArraryTolinkedListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(11);
        arrayList.add(22);
        arrayList.add(33);
        arrayList.add(44);
        arrayList.add(55);
        System.out.println("Arraylist:" + arrayList);
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        linkedList.addAll(arrayList);
        System.out.println(" the linked list elements are : ");
        for (int number : linkedList) {
            System.out.println(number);
        }
    }
}
