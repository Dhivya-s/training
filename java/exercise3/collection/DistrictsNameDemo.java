package com.java.training.core.collection;
/*
Requirement:
    8 districts are shown belowMadurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy
    to be converted to UPPERCASE.
    
Entity:
    Districts
    
Function Declaration:

    public static void main(String[] args)
    
jobs to be done:
    1) Create class Districts and create main method in it.
    2) Create reference for list and assign all values given.
    3) Change all the string value in upper case.
    4) Print the list. 
    
pseudo code

public class DistrictsNameDemo {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Madurai")//add names
        list.replaceAll(String::toUpperCase);
        System.out.println(list);
    }

}

 */


import java.util.Arrays;
import java.util.List;

public class DistrictsNameDemo {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur","Salem", "Erode", "Trichy");
        list.replaceAll(String::toUpperCase);
        System.out.println(list);
    }

}

