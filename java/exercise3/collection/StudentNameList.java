/*
Requirements:

    LIST CONTAINS 10 STUDENT NAMESkrishnan, abishek, arun,vignesh, kiruthiga, murugan,adhithya,balaji,vicky, priya and 
    display only names starting with 'A'.
    
Entities:

    StudentNames
     
Function Declaration:

    public static void main(String[] args)
     
Jobs To Be Done

    1. Create the object list as Generic type and the string values are converted into list.
    2. Inside the list the values are converted into upper case.
    3. Using for each loop and if condition the names are starts with "A" prints the name.

Pseudo Code:
public class StudentNameList {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("krishnan")
        System.out.println(list);
        list.replaceAll(String::toUpperCase);
        System.out.println(list);
        
        for (String names : list) {
            if (names.startsWith("A")) {
                System.out.println(names);
            }
        }
    }
}

*/

package com.java.training.core.collection;

import java.util.List;
import java.util.Arrays;

public class StudentNameList {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("krishnan", "abishek", "arun", "vignesh", "kiruthiga","murugan", "adhithya", "balaji", "vicky", "priya");
        System.out.println("list" + list);
        list.replaceAll(String::toUpperCase);
        System.out.println("Uppercase of the list is" + list);
        
        for (String names : list) {
            if (names.startsWith("A")) {
                System.out.println(names);
            }
        }
    }
}
