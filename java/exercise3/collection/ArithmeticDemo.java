/*Requirement:
    Addition,Substraction,Multiplication and Division concepts are achieved using 
    Lambda expression and functional interface.

Entity:
    LambdaInterfaceDemo

Function Declaration:
    public static void main(String[] args){} 

Jobs To Be Done:
    1)Get the two input from the user
    2)Create an interface ArithmeticOperation and create a method for input
    3)Add the two input using lambda expression
    4)Subtract the two input using lambda expression
    5)Multiply the two input using lambda expression
    6)Inside try block
    7)Divide the two input using lambda expression
    8)if the exception is thrown exception is handled.
      
Pseudo code:
    interface ArithmeticOperation {
        int input(int x,int y);
    }
    
    public class LambdaFuntionalInterface {
        public static void main(String[] args) {
            Get the two input from the user using Scanner
            Add the two input using lambda expression
            Subtract the two input using lambda expression
            Multiply the two input using lambda expression
            try {
               divide the two input using lambda expression.
            } catch(Exception exception) {
                  handle the exception
            }
       }   
    }  
*/

package com.java.training.core.collection;

import java.util.Scanner;

interface ArithmeticOperation {
    
    int values(int number1, int number2);
}

public class ArithmeticDemo {
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Enter the First Number : " );
        int number1 = scanner.nextInt();
        
        System.out.print("Enter the Second Number : ");
        int number2 = scanner.nextInt();
        
        
        ArithmeticOperation addition = (int x, int y) -> (x + y);
        System.out.println("Addition = " + addition.values(number1, number2));

        ArithmeticOperation subtraction = (int x, int y) -> (x - y);
        System.out.println("Subtraction = " + subtraction.values(number1, number2));

        ArithmeticOperation multiplication = (int x, int y) -> (x * y);
        System.out.println("Multiplication = " + multiplication.values(number1, number2));
        
        try {
            
            ArithmeticOperation division = (int x, int y) -> (x / y);
            System.out.println("Division = " + division.values(number1, number2));
            
        } catch (Exception exception) {
            System.out.println("correct values must be inserted to perform division");
        }
      
        scanner.close();
    }
}

