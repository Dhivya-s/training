/*
Requirement:
  To print the sum of the variable arguments.

Entity:
    LambdaVarargDemo

Function Declaration:
    public int printVararg(int ... numbers)
    public static void main(String[] args)
    
Jobs To Be Done:
     1. Create a interface Vararg
     2. Create an object
     3. Declare a method public int printAddition(int ... numbers)
     4. Under a main method Create a lambda expression and call the method and print the result.  

pseudo code:

interface Vararg {

    public int PrintVararg(int ... numbers);
}
public class LambdaVarargDemo {
    public static void main(String[] args) {
        lambda 
        //use lambda and add the numbers 
        int sum = 0;
        for(int values : numbers) {
            sum = sum +values;
        }
        return sum;
    };
    System.out.println("the added number"));
}
*/
 

package com.java.training.core.lambda;

interface Vararg {
    
    public int PrintVararg(int ... numbers);
}
public class LambdaVarargDemo {
    
    public static void main(String[] args) {
        Vararg value = (numbers) -> {
            int sum = 0;
            for(int values : numbers) {
                sum = sum +values;
            }
            return sum;
        };
        System.out.println("The sum value is " + value.PrintVararg(11,22,33,44));
    }
}
