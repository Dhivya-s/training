/*
 Requirement:
    To code the simple lambda expression using the method getValue()
 
 Entity:
    SimpleLambdaDemo
 
 Function Declaration
    public int getValue()
    public static void main(String[] args)

Jobs to be done:
    1  Create a interface LambdaDemo.
    2. Create a object value
    2. Declare a method public int getValue()
    3. Under a main method Create a lambda expression and call the method and print the result.     

pseudo code:

interface Demo {
    public int getvalue();
}
public class SimpleLambdaDemo {
    Lambda 
    //use lambda and get value and print it
    System.out.println("The value is: " + value.getValue());
}
     */



package com.java.training.core.lambda;

interface Demo {
    public int getvalue();
}
public class SimpleLambdaDemo {

    public static void main(String[] args) {
        Demo value = () -> 21;
        System.out.println("The value is " + value.getvalue());
    }
}

