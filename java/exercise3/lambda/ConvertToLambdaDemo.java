/*
Requirement:
    To convert the given Anonymous class to Lambda expression.
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}

Entity:
    public class ConvertToLambdaDemo

Function Declaration:
    public boolean isEven(int value)
    public static void main(String[] args)

Jobs to be done:
    1. Create a interface CheckNumber.
    2. Declare a method public boolean isEven(int value)
    3. Under a main method Create a lambda expression 
       and check the value is odd or even 
    4. call the method and print the result.     

pseudo code:

interface CheckNumber {
    
    public boolean isEven(int value);
}

public class ConvertToLambdaDemo {
    
    public static void main(String[] args) {
        Lambda 
        //use lambda and check odd or even
        
         if (value %2 == 0) {
                return true;
         } else {
                return false;
         }
        
        System.out.println("is the number even? " + evenOrOdd.isEven(11));
    }

}
*
*/




package com.java.training.core.lambda;

interface CheckNumber {
    
    public boolean isEven(int value);
}
public class ConvertToLambdaDemo {
    public static void main(String[] args) {
        CheckNumber OddOrEven = (value) -> {
            if (value %2 == 0) {
                return true;
            } else {
                return false;
            }
        };
        System.out.println("is the number is even ? " + OddOrEven.isEven(11));
    }
}

