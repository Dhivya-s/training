/*
 Requirement :
    write a Lambda expression program with a single method interface
    To concatenate two strings
    
Entity:
     LambdaConcatenateDemo 

Function Declaration:
     public String printConcatenate();
     public static void main(String[] args) 

Jobs to be Done :
    1.Declare interface Concatenate method 
    2.using lambda expression concatenate two strings
    3.Print the result
     
Pseudo code
interface Concatenate {
    
    public String printConcatenate(String word1, String word2);
}

public class LambdaConcatenateDemo {
    
    public static void main(String[] args) {
        Lambda //using lambda (word1 +word2)
        //print the result
        System.out.println(value.printConcatenate("dhivya ","sankaran"));
    }
}
*/
package com.java.training.core.lambda;

interface Concatenate {
    
    public String printConcatenate(String word1, String word2);
}

public class LambdaConcatenateDemo {
    
    public static void main(String[] args) {
        Concatenate value =(word1,word2) -> (word1 + word2);
        System.out.println(value.printConcatenate("dhivya ","sankaran"));
    }
}


