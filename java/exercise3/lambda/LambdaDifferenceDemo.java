/*
Requirement:
    To write a program to print difference of two numbers using lambda expression and the single method interface

Entity:
    LambdaDifferenceDemo

Function Declaration:
    public static void main(String[] args);
    public int printDifference(int Number1, int Number2);

Jobs To be Done:
    1.create a interface Difference
    2.Declare a method as printDifference
    3.pass the parameter to the method
    4.Find the difference (number1-number2)
    5.print the result
 
pseudo code:
interface Difference {
    public int printDifference(parameters)
}
public class  LambdaDifferenceDemo {
    lambda ->
    //using lambda print difference
    (number2- number1)
    System.out.println(parameters);
    }
}
 */
package com.java.training.core.lambda;

interface Difference {
    public String printDifference(String number1, String number2);
}

public class LambdaDifferenceDemo {
    public static void main(String[] args) {
        Difference value =(number1,number2) -> (number2+number1);
        System.out.println(value.printDifference("dhivya","sankaran"));
    }

}
