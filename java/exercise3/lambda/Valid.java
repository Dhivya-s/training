/*
Requirement:
    Tofind which of the given lambda expression is valid.
        (int x, int y) -> x+y; or (x, y) -> x + y

Entity:
    No entity is used here.

Function Declaration:
    There is no function declared here.

Jobs to be done:
    1. Look the following snippet and answer the given question.

Answer:
Both of them are valid lambda expressions if used in a correct context. With the first one 
(int x, int y) -> x+y; we know that the parameters must be of type int.In case of (x, y) -> x + y; 
if used in a correct context type can be inferred from the context in which the lambda expression is
executed.