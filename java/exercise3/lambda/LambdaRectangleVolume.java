/*Requirement
     To print the volume of the rectangle using Lambda Expressions.

Entity:
      LambdaRectangleVolume
       
Function Declaration:
      public double printVolume(double length, double width, double height);
      public static void main(String[] args);
      
jobs to be done :
    1.Create a interface rectangle 
    2.Create a object value 
    3.Declare height,width,height;
    4.Declare method printVolume
    5.Using lambda expression print volume

Pseudo code:
interface Rectangle {
    
    public double printVolume(double length, double width, double height);
}

public class LambdaRectangleVolume {
    
    public static void main(String[] args) {
        Lambda 
        //use lambda expression
        };
        
        System.out.println("The volume of the rectangle is: " + volume.printVolume(12.44, 667.434, 66.987));
    }
}

     
 */


package com.java.training.core.lambda;

interface Rectangle {
    
    public double printVolume(double length, double Width, double height); 
}

public class LambdaRectangleVolume {
    
    public static void main(String[] args) {
        Rectangle value = (double length, double Width, double height) -> (length*Width*height);
        System.out.println("The volume is " + value.printVolume(10.1, 22.2, 7.9));
    }
}
