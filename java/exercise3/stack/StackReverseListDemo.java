/*
Requirement:
    Reverse List Using Stack with minimum 7 elements in list.

Entity:
    StackReverseListDemo

Function Declaration:
     add()
     push()
     pop()
     clear()
     size()

Jobs to be Done :
    1) In stack_and_queue package, importing the array list and stack. 
    2) Creating the StackReverseList class
    3) Creating the list object as generic type.
    4) Appending the list values.
    5) Creating the stack object in generic type. 
    6) Pushing each element of the list to the stack.
    7) Clearing the list elements to append the reversed list. 
    8) Using for loop,adding the popped element from the stack.
    9) printing the reversed Stack list
 
 Pseudo code:
 
 public class StackReverseListDemo {
    
    public static void main(String[] args) {
        List list //use list
        list.add(9)//Add elements
        Stack values //
        for (Integer numbers : list) {
            values.push(numbers);
        }
        
        list.clear();
        int size = values.size();
        for (int i = 0; i < size; i++) {
            list.add(values.pop());
        }
        System.out.println("after reversing " + list);
        
    }
}

 */
package com.java.training.core.stack;

import java.util.*;

public class StackReverseListDemo {
    
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(9);
        list.add(8);
        list.add(7);
        list.add(7);
        list.add(6);
        list.add(5);
        list.add(4);
        System.out.println("before reversing : " + list);
        Stack<Integer> values = new Stack<>();
        for (Integer numbers : list) {
            values.push(numbers);
        }
        
        list.clear();
        int size = values.size();
        for (int i = 0; i < size; i++) {
            list.add(values.pop());
        }
        System.out.println("after reversing " + list);
        
    }
}
