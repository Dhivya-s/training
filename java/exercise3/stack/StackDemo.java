/*
 Requirement:
    Create a stack using generic type and implement
    Push at least 5 elements
    Pop the peek element
    search a element in stack and print index value
    print the size of stack
    print the elements using Stream
    
Entity:
    StackDemo1
    
Function Declaration:
    push()
    pull()
    search()
    size()
    stream()
    peek()
    public static void main(String[] args)
    
Jobs to be Done:
    
    1. Import Stack and Stream package.
    2. Creating a class StackDemo.
    3. Create an object for a Stack as integer.
    4. push elements of the stack as "bulb","fan","light","table","chair","note","pen".
    5. Then printing the peek popped element, printing the index of searched value in stack
       and printing the elements using stream.

Pseudo code:
public class StackDemo {
    public static void main(String[] args) {
        stack//use stack and add values in it
        values.push("pen");
        System.out.println("the stack values are : " + values);
        System.out.println("after using pop()  : " + values.pop());
        System.out.println("after using peek() : " + values.peek());
        System.out.println("the stack values size is : " + values.size());
        System.out.println("the searched value index  : " + values.search("fan"));
        //use stream and print the values
        System.out.print("The Stack elements printed using Stream ");
        stream.forEach(element-> System.out.println( element + " "));
    }
}
 */

package com.java.training.core.stack;

import java.util.*;
import java.util.stream.Stream;

public class StackDemo {
    
    public static void main(String[] args) {
        Stack<String> values = new Stack<>();
        values.push("bulb");
        values.push("fan");
        values.push("light");
        values.push("table");
        values.push("chair");
        values.push("note");
        values.push("pen");
        System.out.println("the stack values are : " + values);
        System.out.println("after using pop()  : " + values.pop());
        System.out.println("after using peek() : " + values.peek());
        System.out.println("the stack values size is : " + values.size());
        System.out.println("the searched value index  : " + values.search("fan"));
        Stream<String> stream = values.stream();
        System.out.print("The Stack elements printed using Stream ");
        stream.forEach(element-> System.out.println( element + " "));
    }
}
