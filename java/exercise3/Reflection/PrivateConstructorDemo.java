package com.java.training.core.Reflection;

/*
Requirement:
    To create a private constructor with main function.
    
Entity:
    PrivateConstructorDemo
    
Function declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create a private method of its class name.
    2. Create an instance of the current class.
    
Pseudo code:

class PrivateConstructorDemo {

    // create private constructor
    private PrivateConstructor() {
        System.out.println(message);
    }

    public static void main(String[] args) {

        PrivateConstructor privateConstructor = new PrivateConstructor();

    }
}

*/

public class PrivateConstructorDemo {

    // create private constructor
    private PrivateConstructorDemo() {
        System.out.println("This is a private constructor.");
    }
     public static void main(String[] args) {
         
         @SuppressWarnings("unused")
         PrivateConstructorDemo privateConstructor = new PrivateConstructorDemo();
    }
}
