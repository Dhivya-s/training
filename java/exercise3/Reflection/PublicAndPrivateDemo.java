package com.java.training.core.Reflection;

/*
Requirements :
    Invoke a public and private method .
   
Entity;
   PublicAndPrivateDemo
   CheckDemo 
   
Function Signature:
   public static void main(String[] args)
   
Jobs to be Done:
    1.Create Two methods PrintMethod and PrintData in CheckDemo class
    2.Create a  class PublicAndPrivateDemo and invoke the PrintMethod and PrintData
      from CheckDemo
    3.Print the result.
 
Pseudo code:

class CheckDemo {

    @SuppressWarnings("unused")
    private void privateMethod() {
        System.out.println("The Private Method called from outside");
    }

    public void printData() {
        System.out.println("The Public Method called from outside ");
    }
}

public class PublicAndPrivateDemo {

    public static void main(String[] args) throws Exception {
        CheckDemo check = new CheckDemo();
        Method method = CheckDemo.class.getDeclaredMethod("privateMethod"); 
        method.setAccessible(true);
        method.invoke(check);
    }
}
 */

import java.lang.reflect.Method;

class CheckDemo {

    @SuppressWarnings("unused")
    private void privateMethod() {
        System.out.println("The Private Method called from outside");
    }
    public void printData() {
        System.out.println("The Public Method called from outside ");
    }
}

public class PublicAndPrivateDemo {

    public static void main(String[] args) throws Exception {
        CheckDemo check = new CheckDemo();
        Method method = CheckDemo.class.getDeclaredMethod("privateMethod"); 
        method.setAccessible(true);
        method.invoke(check);
    }
}
