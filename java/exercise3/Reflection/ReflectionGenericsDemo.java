package com.java.training.core.Reflection;

/*
Requirement:
    To consider this class:
    class Node<T> implements Comparable<T> {
        public int compareTo(T obj) {  }
        // ...
    }
    Check whether the following code compile? If not, why?
            
Entity:
    ReflectionGenericsDemo
    
Function declaration:
    public int compareTo(T obj) {  }
    
Solution:
    The following code will compile.
*/