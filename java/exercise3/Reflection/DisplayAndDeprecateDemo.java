package com.java.training.core.Reflection;

/*
Requirements:
   complete:
   i)Deprecate the display method
   ii)how to suppress the deprecate message 
   
class DeprecatedTestTest 
{   
    //method to be deprecated
    public void Display() 
    { 
        System.out.println("Deprecated test display()"); 
    } 
} 
  
public class Test 
{ 
 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 

Entity:
    DeprecatedTest
    DisplayAndDeprecateDemo 
  
Methods Signature:
     public void Display() 
     public static void main(String[] args) 
 
Jobs To Be Done:
    1.Create DeprecatedTest Class
    2.And invoke the display method of Deprecated class in DisplayAndDeprecateDemo class
    3.Print the result using display method

Pseudo code:
class DeprecatedTest {
    @Deprecated
    public void display() {
        System.out.println("Deprecated  test display() ");
    }
}
public class DisplayAndDeprecateDemo {

    public static void main(String[] args) {
        DeprecatedTest d1 = new DeprecatedTest();
        d1.display();
    }
}
*/

class DeprecatedTest {
    
    public void display() {
        System.out.println("Deprecated test display() ");
    }
}
public class DisplayAndDeprecateDemo {

    public static void main(String[] args) {
        DeprecatedTest d1 = new DeprecatedTest();
        d1.display();
    }
}
