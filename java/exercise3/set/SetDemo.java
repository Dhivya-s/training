/*
 Requirement:
    1.Create a set
    2.Add 10 values
    3.Perform addAll() and removeAll() to the set.
    4.Iterate the set using  Iterator, for-each.
    5.Explain the working of contains(), isEmpty() and give example.
    
Entity:
    SetDemo

Function Declaration:
    public static void main(String[] args)
    add()
    addAll()
    hasNext()
    next()
    removeAll()
    contains()
    isEmpty()

Jobs to be Done:
    1.Create a class SetDemo and create a set 
    2.To Add 10 values
    3.To Perform addAll() and removeAll() to the set.
    4.To Iterate the set using  Iterator, for-each.
    5.To use these methods contains(), isEmpty() 
    6.To print the values

Pseudo code:

public class SetDemo {
    
    public static void main(String[] args) {
        Set  values //use set 
        values.add(1);
        //print using iterator
         Iterator number 
        while (number.hasNext()) {
            System.out.println("the values are " + number.next());
        }
        //print using for each
        for (Integer numbers : values) {
            System.out.println("the numbers are " + numbers);
        }
        // using contains() to check value is present or not 
        
        if (values.contains(0) == false) { 
            System.out.println("not Present");
        }
        
        //using isEmpty() to check set is empty or not
        
        System.out.println(values.isEmpty());
    }   
    
}

 */






package com.java.training.core.set;

import java.util.*;

public class SetDemo {
    
    public static void main(String[] args) {
        Set<Integer> values = new HashSet<>();
        values.add(1);
        values.add(2);
        values.add(3);
        values.add(4);
        values.add(5);
        values.add(6);
        values.add(7);
        values.add(8);
        values.add(9);
        values.add(10);

        //print using iterator
		
        Iterator<Integer> number = values.iterator();
        while (number.hasNext()) {
		    System.out.println("the values are " + number.next());
		}
		
        //print using for each
		
        for (Integer numbers : values) {
            System.out.println("the numbers are " + numbers);
        }
		
        // using contains() to check value is present or not 
        
        if (values.contains(0) == false) { 
		    System.out.println("not Present");
        }
        
        //using isEmpty() to check set is empty or not
        
        System.out.println(values.isEmpty());
	}   
    
}


