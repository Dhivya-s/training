package com.java.training.core.Properties;

/*
Requirement:
    Write a program to add 5 elements to a xml file and print the elements in the xml file using list. 
    Remove the 3rd element from the xml file and print the xml file.
    
Entities:
    XmlProperties
    
Function Signature :
    public static void main(String[] args)
     
Jobs to be done:

   1) Create a properties and create object for Properties as property.
   2) Add elements to the property
   3) Create a object for FileOutputStream and assign a name for xml file.
   4) Store the property in FileOutputStream as xml file.
   5) Print the elements in xml file using list. 
   6) Remove the 3rd element from the xml file.
   7) Print the elements in the xml file.
    
Pseudo code:
 
class XmlProperties {
 
    public static void main(String[] args) throws IOException {
        Properties property = new Properties();
        //Add the element to the property.
        OutputStream output = new FileOutputStream("Number.xml");
        property.storeToXML(output, null);
        property.list(System.out);
        property.remove("3rd element key");
        property.list(System.out);
    }
}   
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class XmlPropertitesDemo {

    public static void main(String[] args) throws IOException {
        Properties property = new Properties();
        property.setProperty("1", "Dhivya");
        property.setProperty("2", "vasanth");
        property.setProperty("3", "neethu");
        property.setProperty("4", "Mahesh");
        OutputStream stream = new FileOutputStream("file.xml");
        property.storeToXML(stream, null);
        System.out.println("Properties stored in xml file successfully");
        property.list(System.out);
        property.remove("3");
        System.out.println("After removing the element from the xml file ");
        property.list(System.out);
    }
}
