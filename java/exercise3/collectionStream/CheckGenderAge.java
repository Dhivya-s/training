/*Requirement:
       Write a program to filter the Person, who are male and age greater than 21
  
 Entity:
    checkGenderAge
 
 Function Declaration:
    public static void main(String[] args);
    public static List<Person> createRoster();
    public Sex getGender();
    public int getAge();
       
Jobs To Be Done:

    1.Call the method createRoaster which returns list
    2.Store the returned list in list.
    3.Iterate the list using stream
    4.The name is filtered if gender is male and age is greater than 21.
    5.Then the filtered name are printed.
    
pseudo code:
    public class CheckGenderAge {
     
        public static void main(String[] args) {
           
            List<Person> list = Person.createRoster();  
            
            use stream iterate the list.
            filter(Gender == Male && Age >21 )
            print the filtered person name
            
       }
    }
 */

package com.java.training.core.collectionStream;


import java.util.List;

public class CheckGenderAge {
    
    public static void main(String[] args) {
        List<Person> list = Person.createRoster();  
        list.stream()
                 .filter(x -> (x.getGender() == Person.Sex.MALE )&&(x.getAge() > 21))
                 .forEach(y -> System.out.println(y.getName()));
    }
}

      