/*
Requirement:
    Sort the roster list based on the person's age in descending order using java.util.Stream
Entity:
    SortDescendingDemo 
    
Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1)Create a object that can access the predefined list in Person.java
    2)Sort the roaster in descending order using stream based on the person age .
    3)convert the newRoaster to stream.
    4)Print the stream using Iteration.
    
    
Pseudo code:
public class   SortDescendingDemo  {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        //sort the roster using stream.
        Stream<Person> stream = newRoster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}

*/
package com.java.training.core.collectionStream;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortDescendingDemo {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = roster.stream().sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.toList());
        Stream<Person> stream = newRoster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}


