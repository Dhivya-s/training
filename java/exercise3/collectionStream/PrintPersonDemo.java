/*
Requirement:
    Print all the persons in the roster using java.util.Stream<T>#forEach 

Entity:
    public class PrintPersonDemo 

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1) Create an object that can able to access the predefined method in Person.java.
    2) Create a reference to the Person Using stream.
    3) Print the Person from Person.java.
    
Pseudo Code:
class PrintPersonDemo  {

    public static void main(String[] args) {
        // Referring from Person.java
        List<Person> roster = Person.createRoster();
        //Create a reference Using stream
        //Print the Person using stream
    }
}
 */
package com.java.training.core.collectionStream;

import java.util.List;
import java.util.stream.Stream;


public class PrintPersonDemo {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}

