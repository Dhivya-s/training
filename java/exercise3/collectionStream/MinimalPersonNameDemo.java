/* 
 Requirement:
    Write a program to print minimal person with name and email address from the Person 
    class using java.util.Stream<T>#map API.
      
Entity:
    MinimalPersonNameDem

 Function Declaration:
    public static void main(String[] args)
    public String getNmae() { }
    public String getEmailAddress() { }
       
 Jobs To Be Done:
    1)Call the method createRoaster which returns list
    2)Store the returned list in list.
    3)Using map in stream print the name of the person
    4)Using map in stream print the email of the person
      
 pseudo code:
   public class MinimalPersonNameDem {

    public static void main(String[] args) {

        List<Person> list = Person.createRoster();  
        System.out.println("Names of the person:");
        Using map in stream print the name of the person
        System.out.println("EmailAddress of the person:");
        Using map in stream print the email of the person
    }
}

 * 
 */

package com.java.training.core.collectionStream;
import java.util.List;

public class MinimalPersonNameDemo {

    public static void main(String[] args) {

        List<Person> list = Person.createRoster();  

        System.out.println("Names of the person:");
        list.stream()
        .map(name -> (name.getName()))
        .forEach(System.out::println);

        System.out.println("EmailAddress of the person:");
        list.stream()
        .map(mail -> (mail.getEmailAddress()))
        .forEach(System.out::println);

    }
}






