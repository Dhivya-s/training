/*
Requirement:
       List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
        - Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
        - Print the number of persons in roster List after the above addition.
        - Remove the all the person in the roster list
        
Entity:
    AddPersonDemo 

MethodSignature:
    public static void main(String[] args) 
  
Jobs to be done:
    1) Create an object that can able to access the predefined list in Person.java.
    2) Create a new list.
    3) Add a new names in the list.
    4) Add all the names in the newly created list to the predefined list.
    5) Print all the names in a predefined list using stream.
    6) Print the size of predefined list.
    7) Clear the list. 
    8) Print the list.
    
Pseudo Code:
  public class AddPerson {

    public static void main(String[] args) {
        // Referring from Person.java
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        //Add the new names to the new list
        
        //Add all the names from new list to predefined list
        roster.addAll(newRoster);
        
        // Stream is created
        // Using Stream
        // list elements are printed using stream
        System.out.println("Number of person in the roster list :" + roster.size());
        
        // clear the list
        roster.clear();
        System.out.println("After removing all elements frfom the list :" + roster);
    }

}
*/

package com.java.training.core.collectionStream;


import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class AddPersonDemo {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE,
                "john@example.com"));
        newRoster.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
        newRoster.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE,
                "bob@example.com"));
        roster.addAll(newRoster);
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
        System.out.println("Number of person in the roster list :" + roster.size());
        roster.clear();
        System.out.println("After removing all elements from the list :" + roster);
    }

}

