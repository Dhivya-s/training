package com.java.training.core.collectionStream;

/*
 Requirement:
    Write a program to find the average age of all the Person in the person List
 
 Entity:
    AverageAge
  
Method Signature:
    public static void main(String[] args)
  
Jobs to be done:
   1) Create an object that can able to access the predefined list in Person.java.
   2) Get the average age of the persons from the predefined list and store it in average which type as double.
   3) Print the average age.
      
Pseudo Code:
  
public class AverageAge {

    public static void main(String[] args) {
        //Referring from Person.java
        List<Person> roster = Person.createRoster();
        //Get the average age and store it in average
        System.out.println("Average age : " + average);

    }
}

*/

import java.util.List;

public class AverageAge {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        double average = roster.stream().mapToInt(Person::getAge).average().getAsDouble();
        System.out.println("Average age  is: " + average);

    }
}