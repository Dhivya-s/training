/*Requirement:
    To find the output of the given program.
    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
    
Entities:
    IdentifyMyParts

Function Declaration:
    There is no function is declared in this program.

Jobs To Be Done:
    1)Considering the given program
    2)Checking the values of x & y values of objects a and b.
    3)Printing the resultant values of each variable.
    4)Identifying the part of x and printing the result
*/
/*
OUTPUT:
a.y = 5
b.y = 6
a.x = 2// x is a static variable
b.x = 2
IdentifyMyParts.x = 2
*/