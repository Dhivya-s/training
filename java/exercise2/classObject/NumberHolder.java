package com.java.training.core.classObject;

/*Requirement:
    To write code that creates an instance of the class and initializes its two member variables
    with provided values,and then displays the value of each member variable.
    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }

Entities:
    NumberHolder

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1)Int and Float is declared as integer and float and decalred as public.
    2)An object is created for NumberHolder as number.
    3)The value is initialized for anInt as 22
    4)The value is initialized for anFloat as 2.2f.
    5)anInt is printed.
    6)anFloat is printed.
*/
public class NumberHolder {
    
    public int anInt;
    public float aFloat;
    public static void main(String[] args) {
    NumberHolder number = new NumberHolder();
    number.anInt = 22;
    number.aFloat = 2.2f;
    System.out.println(number.anInt);   //22
    System.out.println(number.aFloat);   //2.2
    }
}