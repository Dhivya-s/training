package com.java.training.core.classObject;

/*Requirement:
    To correct the error for the given program
 public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
Entities:
    SomethingIsWrong

Function Declaration:
    area();
    public static void main(String[] args);
    

Jobs To Be Done:
   1)Create an object for Rectangle as myRect.
   2)Assign the width as 40.
   3)Assign the height as 50.
   4)Invoke the method area , which returns the area of the rectangle , and print the area of 
     rectangle.
*/

//THE CORRECTED PROGRAM:

public class Rectangle {
    
    public int width;
    public int height;
    public  int area() {
        return (width * height);
    }
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());//output 2000
    }
}    

