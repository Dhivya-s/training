/*Requirement:
    To find the class and instance Variables in the given program.
public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    
Entities:
    IdentifyMyParts

Function Declaration:
    There is no function is declared in this program.

Jobs To Be Done:
    1)Considering the given class from the question.
    2)Finding the class variable(variable with static).
    3)Answering for the question.
    4)Finding the instance Variable.(other then static variables)
    5)Answering for the question.
*/
/*
Class Variable : x //(static int x = 7)
Instance Variable : y//(int y = 3)
*/