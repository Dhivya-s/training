package com.java.training.core.Inheritance;

/*
Requirement:
    To demonstrate inheritance, overloading, overriding using 
    Animal, Dog, Cat and Snake class of objects 

Entities: Animal, Dog, Cat ,Snake and InheritanceDemo.

Function Declaration:
    move()
    move(int km)
    animalSound()
    public static void main(String[] args).

Jobs to be done:
    1) Declare a parent class as Animal and child class as 
       Animal, Dog, Cat ,Snake and main class as InheritanceDemo.
    2) Declare a mentioned functions in the child class and 
       declare the print statement which you want to print
    3) create the object for all class and invoke the functions using those 
       objects
    4) If the function move() is present in child class it will execute 
    5) Else it will search for function move() in parent class and execute that (over loading)
    6) The function animalSound() is also present in both Dog and Cat so animalSound()in Dog
       override the animalSound.in Cat in same way every line executes.    
*/
    
public class InheritanceDemo {
    public static void main(String[] args){
        Animal animal = new Animal();
        Dog dog = new Dog();
        Cat cat = new Cat();
        Snake snake = new Snake();
        animal.animalSound();
        animal.move();
        animal.move(7);
        cat.animalSound();
        cat.move();
        cat.move(6);
        dog.animalSound();
        dog.move();
        dog.move(8);
        snake.animalSound();//overriding
        snake.move();
        snake.move(9);//overloading
    }
}
        