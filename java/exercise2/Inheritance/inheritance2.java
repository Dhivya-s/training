/*
Requirement:
    To find  Which method overrides a method in the superclass? 
    Which method hides a method in the superclass? 
    What do the other methods do? 
    by Consider the following two classes:
    
    public class ClassA {
    public void methodOne(int i) {
    }
    public void methodTwo(int i) {
    }
    public static void methodThree(int i) {
    }
    public static void methodFour(int i) {
    }
}

public class ClassB extends ClassA {
    public static void methodOne(int i) {
    }
    public void methodTwo(int i) {
    }
    public void methodThree(int i) {
    }
    public static void methodFour(int i) {
    }
}
    
Entities:
    ClassA and ClassB are the entities used

Function Declaration:
       methodOne(int i) 
       methodTwo(int i) 
       methodThree(int i)
       methodFour(int i) 

Jobs to be done :

Question 1a: Which method overrides a method in the superclass?
Answer 1a: methodTwo

Question 1b: Which method hides a method in the superclass?
Answer 1b: methodFour

Question 1c: What do the other methods do?
Answer 1c: They cause compile-time errors.
*/