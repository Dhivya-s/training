package com.java.training.core.Inheritance;

public class Snake extends Animal {
    
    public void animalSound() {
        System.out.println("the snake crawls");
    }
    public void move() {
        System.out.println("the snake is moving");
    }
}