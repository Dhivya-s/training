package com.java.training.core.Inheritance;

public class Cat extends Animal {
    
    public void animalSound() {
        System.out.println("cat sounds as mewoo");
    }
    public void move() {
        System.out.println("the cat is moving ");
    }
}