package com.java.training.core.Inheritance;

public class Animal {
    
    public void animalSound() {
        System.out.println("enter the animal name :");
    }
    public void move() {
        System.out.println("the animal not yet slept it is moving");
    }
    public void move(int km) {
        System.out.println("the animal is " + km +"km away");
    }
}