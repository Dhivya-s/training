package com.java.training.coreControlFlow;

/*
Requirement:
    To find fibonacci series using Recursion.

Entity:
    FibonacciUsingRecursion 

Function Declaration:
    public static void fibonacci(int i);
    public static void main(String[] args);

Jobs to be Done:
    1. Get input number from user 
    2. Declare the variables and initialize 0 for number1, 1 for number2 
    3. Invoke the method fibonacci in the class FibonacciRecursion and pass limit as the parameter.
       3.1) Check the limit is greater than 0.
       3.2) If the condition satisfied 
    4. Add the values of number1 and number2 and store it in new variable sum.
    5. Assign the value of number2 to number1 and the value of number2 to sum.
    6. print number1.
       6.1)Invoke the fibonacci method from FibonacciRecursion and reduce the limit by one and pass 
          as parameter.
*/

import java.util.Scanner;

public class FibonacciWithRecursion {
    public static int number1 = 0;
    public static int number2 = 1;
    public static int sum;
    public static void fibonacci(int i) {
        if(i > 0) {
            sum = number1 + number2;
            number1 = number2;
            number2 = sum;
            System.out.print(number1+ " ");
            fibonacci(i - 1);
        }
    }
    public static void main(String[] args) {
        int number;
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
        fibonacci(number);
        scanner.close();
    }
}