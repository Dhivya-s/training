package com.java.training.coreControlFlow;

/*
Requirement:
    To find fibonacci series using for loop.

Entity:
    Fibonacci 

Function Declaration:
     public static void main(String[] args);
     
Jobs to be Done:
    1. Get input number from user
    2. Declare the variables and initialize 0 for number1, 1 for number2 
    3. Then use for loop. Assign the value to the new variable i. 
       3.1) And initialize 1 for it.
       3.2) check if the number is greater than or equal to number
       3.3) If the condition satisfied 
    4. Add the values of number1 and number2 and store it in new variable sum.
    5. Assign the value of number2 to number1 and the value of number2 to sum.
    6. print number1.
*/

import java.util.Scanner;

public class Fibonacci {
    
    public static void main(String[] args) {
        int number;
        int number1 = 0;
        int number2 = 1;
        int sum;
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
        for(int i = 1; i <= number; i++) {
            sum = number1+ number2;
            number1 = number2;
            number2 = sum;
            System.out.print(number1 + " ");
        }
        scanner.close();
    }
}