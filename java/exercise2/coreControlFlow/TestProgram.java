package com.java.training.coreControlFlow;

/*
Requirement:
    To write the test program consisting the following codes snippet and make the value of aNumber
    to 3 and find the output of the code by using proper line space amd breakers and also by using
    proper curly braces. The following snippet is given as
     if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
     else System.out.println("second string");
     System.out.println("third string");

Entities:
    TestProgram

Function Declaration:
    There is no function declared in this program.

Jobs To Be Done:
    1. create a class Test.
    2. Create a variable aNumber of type int.
    3. Assign the value for aNumber as 3.
    4. Use proper line space and use proper curly braces in the given snippet as given below
    5. Print the output
*/

import java.util.Scanner;

public class TestProgram {
    
    public static void main(String[] args) {
        int aNumber;
        Scanner scanner = new Scanner(System.in);
        aNumber = scanner.nextInt();
        if(aNumber >= 0) {

            if(aNumber == 0) {
                System.out.println("First String");
            }
        } else {
            System.out.println("Second String");
        }
        System.out.println("Third String");
        scanner.close();
    }
}

/*output:
Third String */