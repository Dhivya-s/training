/*Requirement:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string


Entities:
    There is no class name is mentioned


Function Declaration:
    there is no function is declared in this program.

Jobs To Be Done:
    1)considering the given program
    2)checking aNumber is greater than or equal to zero 
    3)It is true so it moves to next statement
    4)checking that aNumber is equal to zero 
    5)It is flase so it skip the statement and moves to else part 
    6)There is no else part so it moves to previous if's else part
    7)so it print the previous else print statement second string
    8)Followed by third string

*/
/*
Output:
second string 
third string
*/
