package com.java.training.coreControlFlow;

/*Requriement
    To find fibonacci using while loop 

Entites:
    FibonacciWithWhile
    
Function Declaration:
     public static void main(String[] args)

Jobs to be done:
    1) Get input number from user
    2) Declare the variables and initialize 0 for number1, 1 for number2 
    3) Then use the while loop declare a variable i and assign value as 1
       3.1) Check the number is greater than i
       3.2) If the condition satisfied 
    4) Add the values of number1 and number2 and store it in new variable sum.
    5) Assign the value of number2 to number1 and the value of number2 to sum and then increments the value of i.
    6) print number1.

*/
import java.util.Scanner;

public class FibonacciWithWhile {
    
    public static void main(String[] args) {
        int number1;
        int number2;
        int sum; 
        int number;
        int i;
        number1 = 0;
        number2 = 1;
        i = 1;
        Scanner scanner= new Scanner(System.in);
        number = scanner.nextInt();
        while(i <= number){
            sum = number1 + number2;
            number1 = number2;
            number2 = sum;
            i++;
            System.out.print(number1 + " ");
        }
        scanner.close();
    }
}