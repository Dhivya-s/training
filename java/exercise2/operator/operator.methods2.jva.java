/*Requirement:
    To explain why the value "6" is printed twice in a row:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }
Entities:
    Here the class used is PrePostDemo

Function declaration:
     There is no function declared in this program.

Jobs to be done:

     1)First the class is created 
     2)Then the value of i is initialized (i=3)
     3)Then the value of i is post incremented 
     4)Now the value of i is printed (i=4)
     5)Then the value of i is preincremented 
     6)Then the value of i is printed (i=5)
     7)Then the value of i is preincremented and printed (i=6)
     8)Now the value of i is postincremented so the same value of i is get printed(i=6) 
     9)then it will increse the i value(i=7)
     10)NOW the value of i is (i=7) get printed
*/