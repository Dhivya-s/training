package com.java.training.core.Vararg;

/*
Requirement:
    To demonstrate overloading with varArgs 
  
Entities:
    DemoVararg,VarargDemo

Function Declaration:
    printNumbers(int... number) 
    printStrings(String... word)
    PrintBoolean(Boolean... statement)
    public static void main(String[] args)

Jobs to be done:
    1)creat a class and declare the class name as VarargDemo
    2)Declare the method as printNumbers and declare the data type int 
      and three dots to define vararg and number is the variable name
    3)use for each loop and declare the data type int and
     store it in variable i and then print i 
    4)similarly for printStrings and printBoolean
    5)create a main class as DemoVararg
    6)create a object for the VarargDemo as vararg 
      and invoke the methods by giving inputs using that object.
*/

public class DemoVararg {
    
    public static void main(String[] args) {
        VarargDemo vararg = new VarargDemo();
        vararg.printNumbers(1,2,3,4,5,6,7,8,9,10);
        vararg.printStrings("Dhivya","Aishu","Murugeswari");
        vararg.PrintBoolean(true,false,true);
    }
}
