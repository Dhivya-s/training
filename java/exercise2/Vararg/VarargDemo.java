package com.java.training.core.Vararg;

public class VarargDemo {
    
    public void printNumbers(int... number) {
        for(int i : number) {
        System.out.println("printnumbers = " + i);
        }
    }
    public void printStrings(String... word) {
        for(String s : word) {
            System.out.println("printstring = "+ s);
        }
    }
    public void PrintBoolean(Boolean... statement) {
        for(Boolean b : statement) {
            System.out.print("PrintBoolean = " + b);
        }
    }
}