package com.java.training.coreAbstract;

/*
Requirement:
    To demonstrate the abstract class using the class shape that has two methods to calculate the
    area and perimeter of two classes named the Square and the Circle extended from the class Shape.

Entity:
    AbstractDemo 
    Shape.
    Square.
    Circle.

Function Declaration:
    public void printArea();
    public void print Perimeter();
    public static void main(String[] args).

Jobs to be done:
     1)Get the side as input from the user.
    2)Get the radius as input from the user.
    3)Pass the side as parameter for the constructor in the class Square.
      3.1)assign the value of side to side in entity square.
    4)Pass the radius as parameter for the constructor in the class Circle.
      4.1)assign the value of radius to radius in entity Circle.
    5)Invoke the method printArea from the abstract class Shape for the class Square .
      5.1)print the area of the square . 
    6)Invoke the method printPerimeter from the abstract class Shape for the class Square .
      6.1)print the perimeter of the square .
    7)Invoke the method printArea from the abstract class Shape for the class Circle .
      7.1)print the area of the square . 
    8)Invoke the method printPerimeter from the abstract class Shape for the class circle .
      8.1)print the perimeter of the Circle .
    
*/
import java.util.Scanner;

public class AbstractDemo {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double side = scanner.nextDouble();
        double radius = scanner.nextDouble();
        Square square = new Square(side);
        square.printArea();
        square.printPerimeter();
        Circle circle = new Circle(radius);
        circle.printArea();
        circle.printPerimeter();
        scanner.close();
    }
}
