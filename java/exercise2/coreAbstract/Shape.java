package com.java.training.coreAbstract;

public abstract class Shape {
    
    public abstract void printArea();
    public abstract void printPerimeter();
}