package com.java.training.core.java.lang;

/*
Requirement:
    To demonstrate object equality using Object.equals() vs ==, using String objects.
    
Entitites:
    ObjectEqualDemo

Function Declaration:
    Object.equals()

Jobs to be done :
    1}Declare a class called ObjectEqualDemo
    2)Create an String object
    3)Print using == it give result as false
      because the address are different
    4)print using Object.equals it gives true as output 
*/

public class ObjectEqualDemo {
    
    public static void main(String[] args) {
        String word1 = new String("dhivya");
        String word2 = new String("dhivya");
        System.out.println(word1 == word2);
        System.out.println("dhivya = dhivya : " +word1.equals(word2));
    }
}    

