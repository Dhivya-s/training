package com.java.training.core.java.lang;


/*
Requirement:
    To print the absolute path of the .class file of the current class.

Entity:
    CurrentDirectory.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1. Declare the class CurrentDirectory.
    2. Under the main method declare a variable called path of type String, this would get a current
       user directory of a java class file.
    3. Print that directory.
*/
public class CurrentDirectory {

    public static void main(String[] args) {
        String path = System.getProperty("user.dir");
        System.out.println("Working Directory = " + path);
    }
}