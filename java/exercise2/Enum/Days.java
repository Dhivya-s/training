package com.java.training.core.Enum;

/*
Requirement:
    To compare the enum values using equal method and == operator.
    
Entities:
    Days,EnumDemo

Function Declaration:
    public static void main (String[] args)

Jobs to be done:
    1) Declare enum class as Days and enum values as 
       MONDAY,TUESDAY,WEDNESDA,THURSDAY,FRIDAY,SATURDAY,SUNDAY.
    2) Declare a main class as EnumDemo 
    3) store the value THURSDAY in the variable day
    4) check If the day in Days is equals to THURSDAY 
       4.1) if the condition satisfied then print the statement
       4.2) if the condition not satisfied check it till the condition become true.
   
    
*/

public enum Days {MONDAY
                 ,TUESDAY
                 ,WEDNESDAY
                 ,THURSDAY
                 ,FRIDAY
                 ,SATURDAY
                 ,SUNDAY
}

