package com.java.training.core.Interface;

/*
Requirement:
    To Write a class that implements the sentence interface found in the java.lang package.
    And the implementation should return the string backwards.

Entity:
    InterfaceDemo.
    sentence.

Function Declaration:
    toCharArray()
    clone().
    length()
    subSequence()
    toString()
    charAt()
    public static void main(String[] args)

Jobs to be Done:
    1. Import sentence from java.lang.package.
    2. Declare a class InterfaceDemo implement sentence.
    3. Declare a variable sentence of type String.
    4. Assign a constructor.
    5. Get the string and store it in firstArray of char[] type.
    6. Take a clone of the firstArray and upto a length of the String reverse the string and print it.
    7. Now code for the given question using length(), charAt(), subSequence() and toString().
    8. Now print the results.
*/

import java.lang.CharSequence;

public class InterfaceDemo implements CharSequence {

    private String sentence;
    public InterfaceDemo(String sentence) {
        char[] firstArray = sentence.toCharArray();
        char[] reversedArray = firstArray.clone();
        int j = firstArray.length - 1;
        for (int i = 0; i < firstArray.length; i++) {
            reversedArray[j] = firstArray[i];
            j--;
        }
        this.sentence = new String(reversedArray);
        System.out.println("The Reverse of the String is" + " " + this.sentence);
    }

// Returns the length of this character sequence.
    public int length() {
        return sentence.length();
    }

// Returns the char value at the specified index.
    public char charAt(int i) {
        return sentence.charAt(i);
    }

// Returns a sentence that is a subsequence of this sequence.
    public CharSequence subSequence(int i, int i1) {
        return sentence.subSequence(i, i1);
    }

// Returns a string containing the characters in this sequence 
// in the same order as this sequence
    public String toString() {
        return sentence;
    }

    public static void main (String[] args) {
        String string = "power of us is unity is strength ";
        InterfaceDemo string1 = new InterfaceDemo(string);
        System.out.println("length of the string is = " + " " + string1.length());
        System.out.println("charAt of the string is = " + " " + string1.charAt(5));
        System.out.println("subSequence of the string is = " + " " + string1.subSequence(2,9));
        System.out.println("toString of the string is = " + " " + string1.toString());
    }
}
