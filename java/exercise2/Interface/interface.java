/*
Requirement:
    To analyze the following interface valid or not:
    public interface Marker {}

Entity:
    No Entity.

Function Declaration:
    No function declaration.

Jobs To Be Done:
    1.Analyse the given expression.
    2.To tell is this interface is valid or not.
*/
/*
Answer:
    Yes, This interface is valid.
*/