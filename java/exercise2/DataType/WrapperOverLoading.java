package com.java.training.core.DataType;

/*
Requirement:
    To demonstrate overloading with Wrapper types.

Entity:
    WrapperOverLoading

Function declaration:
    check(Integer),
    check(Float),
    check(Double)
    check(Long)
    public static void main(String[] args)

Jobs to be done:
    1.invoke the class WrapperOverLoading.
    2.Declare the methods as check with different wrapper types.
    3.Create an object for the class and pointer as value.
    4.invoke the methods with different data type values.
    5.Then Print the values to wrapper types.
*/
//Program:
public class WrapperOverLoading {
    
    public void check(Integer number) {
        System.out.println("integer value is :" + number);
    }
    public void check(Float number) {
        System.out.println("float vale is : "+ number);
    }
    public void check(Double number) {
        System.out.println("double value is "+ number);
    }
    public void check(Long number) {
        System.out.println("long value is "+ number);
    }
    public static void main(String[] args) {
        WrapperOverLoading value = new WrapperOverLoading();
        value.check(new Integer(22));
        value.check(new Float(2.32f));
        value.check(new Double(2.222222222d));
        value.check(new Long(4676532782L));
    }
}
