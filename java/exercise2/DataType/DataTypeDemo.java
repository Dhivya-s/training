package com.java.training.core.DataType;

/*
Requirement:
    To print the type of the result value of following expressions
    100 / 24
    100.10 / 10
    'Z' / 2
    10.5 / 0.5
    12.4 % 5.5
    100 % 56.
 
Entities:
    DataTypeDemo
    
Function Declaration:
    
    public static void main(String[] args)
    
Jobs to Be Done:
    1.Invoke the  class DataTypeDemo.
      1.1) Declare objects to each expressions.
    2.Print the type of result value by using methods getClass() and getName().
*/
    
public class DataTypeDemo {
    
    public static void main(String[] args) {
        Object number1 = 100 / 24;
        System.out.println(number1.getClass().getName());
        Object number2 = 100.10 / 10;
        System.out.println(number2.getClass().getName());
        Object number3 = 'Z' /2;
        System.out.println(number3.getClass().getName());
        Object number4 =10.5 /0.5 ;
        System.out.println(number4.getClass().getName());
        Object number5 = 12.4 % 5.5;
        System.out.println(number5.getClass().getName());
        Object number6 = 100/56;
        System.out.println(number6.getClass().getName());
    }
}
