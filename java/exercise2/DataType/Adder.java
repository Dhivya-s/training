package com.java.training.core.DataType;

/* 
Requirement
    Create a program that reads an unspecified number of integer arguments from 
    the command line and adds them together.    
    For example, suppose that you enter the following: java Adder 1 3 2 10   
    The program should display 16 and then exit. The program should display an error message 
    if the user enters only one argument. 

Entity:
    Adder

Function Declaration:
    public void add(int... array)
    public static void main(String[] args)

Jobs to done :
    1) Invoke a method add From Adder class 
       1.1) check if the number is less than one 
    2) If the condition satisfied 
       2.1) print error enter more than one value
    3) If the condition not satisfied 
       3.1) Declare a variable as sum and assign a value as 0 
       3.2) using for each add the sum and number and store it in sum 
       3.3) Add print the sum.
    4) invoke the Adder class and create the object using that pass the parameters to the add Method
*/
//program:
public class Adder {
    
    public void add(int...array) {
        if (array.length < 1) {
            System.out.println("error enter more than one value");
        } else {
            int sum = 0;
            for(int number :array ) {
                sum += number;
            }
            System.out.println(sum);
        }
    }
    public static void main(String[] args) {
        Adder value = new Adder();
        value.add (1,2,3,10);
    }
}

    