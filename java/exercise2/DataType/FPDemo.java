package com.java.training.core.DataType;

/*
Requirement:
    To create a program that is similar to the previous one but instead of 
    reading integer arguments,it reads floating-point arguments.
    It displays the sum of the arguments, using exactly two digits
    to the right of the decimal point.

Entity:
    FPDemo.

Function declaration:
    public static void main(String[] args).

Jobs to be done:
    1.Import the package java.text.DecimalFormat.
    2.invoke a class FPDemo.
    3.Declare a main method declare the variable number1 and number 2 
      to store length of the arguments.
    4.check if the length of the arguments is more than
      two then print the sum of all the arguments.
    5.If the condition is not satisfied 
      than two show the error message.
    6.Then format the output and print the value.
*/

//Program:
import java.text.DecimalFormat;

public class FPDemo {
    
    public static void main(String[] args) {
        float number1 = Float.parseFloat(args[0]);
        float number2 = Float.parseFloat(args[1]);
        if (args.length < 2) {
            System.out.println("Error Enter more than two numbers");
        } else {
        double sum = 0.0;

        for (int i = 0; i < args.length; i++) {
            sum += Double.valueOf(args[i]).doubleValue();
        }

        DecimalFormat value = new DecimalFormat("###,###.##");
        String outputValue = value.format(sum);
        System.out.println(outputValue);
        }
    }
}