package com.java.training.core.DataType;

/*
Requirement:
    To invert the value of a boolean,using an operator.

Entity:
    BooleanDemo

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1.Invoke the class BooleanDemo and
    2.Check whether the two strings are equal or not .
    3.Print the inverted boolean value
*/

//Program:
public class BooleanDemo {
    
    public static void main(String[] args) {
        boolean value = !("Dhivya" == "Dhivya");   //logical complement operator(!)is used to invert
        System.out.println("Inverted boolean value is = : " + value);
    }
}