package com.java.training.core.DataType;

/*
Requirement:
    To print the Class names of the primitive data type.

Entity:
    PrimitiveClassName

Function declaration:
    public static void main(String[] args)
    getName()

Jobs to be done:
    1.Invoke the class PrimitiveClass
    2.Declare a variable to store the class name by using getName() method.
    3.Then Use it for various data type.
    4.Print the class names.
*/

//Program:
public class PrimitiveClass {
    
    public static void main(String[] args) {
        String class1 = int.class.getName();
        System.out.println("Class Name of Integer : " + class1);

        String class2 = char.class.getName();
        System.out.println("Class Name of Character: " + class2);

        String class3 = double.class.getName();
        System.out.println("Class Name of Double : " + class3);

        String class4 = float.class.getName();
        System.out.println("Class Name of Float : " + class4);
    }
}
