/*
Requirement:
  To Use the Java API documentation for the Box class (in the javax.swing package) 
   to help you answer the following questions.    
 - What static nested class does Box define?    
 - What inner class does Box define?    
 - What is the superclass of Box's inner class?    
 - Which of Box's nested classes can you use from any class?    
 - How do you create an instance of Box's Filler class?

Entities: 
    Box

Function Declaration:
    no function

Jobs to be done :
    To answer the above questions
   1) Box.Filler
   2)Box.AccessibleBox
   3)[java.awt.]Container.AccessibleAWTContainer
   4)Box.Filler
   5)new Box.Filler(minDimension, prefDimension, maxDimension)
*/


