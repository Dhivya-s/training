package com.java.training.core.java.doc;

/*
Requirement:
    To find the Integer method would be used to convert a string expressed in base 5 into the
    equivalent int

Entity:
    IntegerMethod.

Function Declaration:
    public static void main(String[] args).
    valueOf().

Jobs to be Done :
    1. Get input string from user 
    2. Declare the variable to equivalentInteger of type int whose String value to be converted
       in base 5 of equivalent int by using the function valuesOf().
    3. Print the the value stored in the equivalentInteger.
*/

import java.util.Scanner;

public class IntegerMethod {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        int equivalentInteger = Integer.valueOf(string,5);
        System.out.println("equivalent int = " + equivalentInteger);
        scanner.close();
    }
}
