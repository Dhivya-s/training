package com.java.training.core.java.doc;

/*
Requirement:
    To find a Double method can be used to detect whether a floating-point number has the special
    value Not a Number (NaN).

Entity:
    NotANumberMethodDemo.

Function Declaration:
    public static void main(String[] args).
    isNaN()

Jobs to be Done:
    1. Declare the class NotANumberMethodDemo.
    2. Under a main method declare and assign two double expressions to be checked for a not a value
    3. Then use isNaN method print the results.
*/

public class NotANumberMethodDemo {
    
    public static void main(String[] args) {
        Double number = new Double(2.0 / 0.0);
        Double number1 = new Double(0.0 *1.0);
        System.out.println(number + " = " + number.isNaN());
        System.out.println(number1 + " = " + number.isNaN());
    }
}
