/*
Requirement:
    To tell What methods would a class that implements
    the java.lang.CharSequence interface have to implement? 

Entities: 
    CharSequence

Function Declaration:
    charAt()
    length()
    subSequence()
    toString()

Jobs to be done:
    To answer the above question
Answer:
    1)charAt()
    2)length()
    3)subSequence()
    4)toString()
*/

    