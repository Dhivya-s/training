/*
Requirement 
In the following program, called ComputeResult, what is the value of result after each numbered 
  line executes?
    public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

            result.setCharAt(0, original.charAt(0));
            result.setCharAt(1, original.charAt(original.length()-1));
            result.insert(1, original.charAt(4));
            result.append(original.substring(1,4));
            result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }

EntitY :
    ComputeResult

Function Declaration:
    public static void main(String[] args)
    charAt(), length(), substring(), setCharAt(), insert(), indexOf(), append().

Jobs to be done :
    1)Declare a class called ComputeResult
    2)Declare a variable name as original with the data type string
    3)Create an object for a string and name it as result
    4)Find the index of a original
    5)Now setCharAt of result of index 0 as original .charAt index 0
    6) First line sets the first character of the string original 'h' to 's'.
    7) Second,i is removed and e is set as 'i' is the character in the final result value.
    8) Third, w is inserted at first index.
    9) Forth, oft is appended to the result.
    10) Last, ar is added in second index and a space is added and the remaining character gets
       printed
*/
/*
Answer:

1) si
2) se
3) swe
4) sweoft
5) swear oft
*/
