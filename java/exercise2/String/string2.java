 /*Requirement:
    To analyze the following code and to check the number of reference to those objects exist after
    the code executes.And also checking whether the object is eligible or garbage collection.
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;
Entity:
    String
Function declaration:
    No function 

Jobs to be done:
    1)A String class is declared ,its object is students ,its array size is 10.
    2)studentName variable is initialized assigned the value as Peter Parker
    3)The studentName is assigned as Peter Parker at index 0 to the array.
    4)Then the studentName is assigned as null.
*/
/*
ANSWER:
    Here the "reference is 1" and it is "Peter Parker".
    It is neither eligible nor garbage collection. Because the studentName is assigned as null, so
    there is no value is initialized after this.
    But the array size is 10 and its has only one reference value.
*/