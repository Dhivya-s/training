package com.java.training.core.String;

/*
Requirement:
   To sort and print following String[] alphabetically ignoring case.
   Also convert and print even indexed Strings into uppercase   
   {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }.

Entities:
   Arrays and Sort are the class used.

Function Declaration:
   no function is declared

Jobs to be done 
  1) First import Array class and declare the class as Sort 
  2) Declare the given strings and assign those to a variable name called place
  3) sort the array and print it 
  4) Then using for loop assign value to i and 
     i greater than the length of sorted array increase the value of i
     it continue till the loop ends
  5) If the index is even print it in upper case
     using the condition (i modules 2) and (touppercase)
  6) Print the result
*/

import java.util.Arrays; 

public class Sort{
    
    public static void main(String[] args){
        String [] place = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(place);
        System.out.println(Arrays.toString(place));
        for(int i=0;i<place.length;i++)
        {
            if( i % 2 == 0) {
                place[i] = place[i].toUpperCase();
            }
        }
        System.out.print(Arrays.toString(place));
    }
}
