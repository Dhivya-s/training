package com.java.training.core.String;

/*
Requirement:
    To write a program that computes my initials of my full name and displays them.

Entity:
     Here the class MyInitials is used here.

Function Declaration:
    Here there is no function declared.

Jobs to be Done:
    1. Declare the class MyInitial.
    2. Declare a variable called name of type String.
    3. Assign the value to the variable name.
    4. Our aim is to print the initials of my name that is to print all the capital letters after
        a space so leave a space before the name starts by using the statement name = " " + name
    5. After every space we should display the first capital letters of the name so use for loop
        until the condition name.length() meets.
    6. If the condition satisfies then execute the condition if character in that
       String name is capital or not.
    7. If the condition is true then print that letter. This process is continued till the condition
       in the for loop fails.
*/

import java.util.Scanner;

public class Initial {
    
    public static void main(String[] args) {
        String name;
        Scanner scanner = new Scanner(System.in);
        name = scanner.nextLine();
        for( int i = 0; i < name.length(); i++) {
            if(name.charAt(i) == ' ' && Character.isUpperCase(name.charAt(i+1))) {
                System.out.println("enter your name");
                System.out.print(name.charAt(i+1) + "is your initial");
            }
        }
        scanner.close();
    }
}
