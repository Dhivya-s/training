import com.bank.training.core.BankAccount;

public class Saving extends BankAccount {
    
    protected long savingAmount;
    
    public Saving(String name, int id, long savingAmount) {
        super(name, id);
        this.savingAmount = savingAmount;
    }
    public void deposite() {
        if(savingAmount == 0) {
            System.out.println("No amount is deposited in your account");
        } else {
            System.out.println(this.savingAmount + " " + "is in your account");
        }
    }
    
    public static void main(String[] args) {
        BankAccount bank = new Saving( 1,234566, "dhivya",50000);
        BankAccount customer = new Saving(34,4342567,"neethu",64578);
        bank.desposite();
        customer.deposite();
    }
}