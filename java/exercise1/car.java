class Vehicle {
    
    protected String brand = "firefox";
    public void design() {
    System.out.println("my favourite design");
  }
}

class Car extends Vehicle {
    private String modelName = "hero";
    public static void main(String[] args) {
    Car myFastCar = new Car();
    myFastCar.design();
    System.out.println(myFastCar.brand + " " + myFastCar.modelName);
  
  }
}
