class OuterClass {
    
    public void name() {
        System.out.println(" enter your name");
    }
    class InnerClass {
        public void address() {
        System.out.println("enter your address");
        }
    }
}
public class Main {
    
    public static void main (String[] args) {
        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass innerClass = outerClass.new InnerClass ();
        outerClass.name();
        innerClass.address();
    }
}