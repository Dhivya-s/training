import com.university.training.core.Student;

public class StudentDetail {
    
    public static void main(String[] args) {
        StudentDetail.studentDetail = new StudentDetail();
        System.out.println("studentName=" + studentDetail .studentName);//error because (private Access Modifier)The code is only accessible within the declared class
        System.out.println("studentAge=" +  studentDetail .studentAge);//error because (private Access Modifier)The code is only accessible within the declared class
        System.out.println("EmployeeMail=" + studentDetail .studentMailId);//error because (private Access Modifier)The code is only accessible within the declared class
    }
}