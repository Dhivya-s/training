import java.util.Scanner;

public class MyMain {
    
    public static void main ( String[] args) {
        double salary ;
        String name ;
        int age;
        Scanner scanner = new Scanner(System.in);
        
        salary = scanner.nextDouble();
        name = scanner.next();
        age = scanner.nextInt();
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Salary: " + salary); 
    }
}