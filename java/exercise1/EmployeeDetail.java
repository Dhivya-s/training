import com.management.training.core.Employee;

public class EmployeeDetail {
    
    public static void main(String[] args) {
        Employee person = new Employee();
        System.out.println("EmployeeName=" + person.employeeName);
        System.out.println("EmployeeAge=" + person.employeeAge);
        System.out.println("EmployeeMail=" + person.employeeMailId);
    }
}