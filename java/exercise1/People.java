public class People {
  
    static void work() {//Static methods can be called without creating objects
        System.out.println("my work is ready");
    }

  
    public void rest() {//Public methods must be called by creating objects
        System.out.println("work hard");
    }

  
    public static void main(String[] args) {
        work(); 
        People people = new People();
        people.rest(); 
    }
}