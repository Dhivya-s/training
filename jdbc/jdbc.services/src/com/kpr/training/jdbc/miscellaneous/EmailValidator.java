package com.kpr.training.jdbc.miscellaneous;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.kpr.training.jdbc.Constant.QueryStatement;
import com.kpr.training.jdbc.Execption.AppException;
import com.kpr.training.jdbc.Execption.ErrorCode;


public class EmailValidator {

    public static boolean mailValidation(String email) {
        String emailRegex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }
    
    public static boolean isUnique(String email, PreparedStatement ps, Connection con) {
        ResultSet result;
        boolean unique = true;
        try {
            ps = con.prepareStatement(QueryStatement.emailUnique);
            ps.setString(1, email);
            result = ps.executeQuery();
            if(result.next()){
                if(!(result.getLong("id") > 0)) {
                    unique = true;
                } else {
                    unique = false;
                }
            }
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
        return unique;
    }

    public static boolean isUniqueExceptID(long id, String email, PreparedStatement ps, Connection con) {
        ResultSet result;
        boolean unique = true;
        try {
            ps = con.prepareStatement(QueryStatement.emailUnique);
            ps.setString(1, email);
            result = ps.executeQuery();
            if(result.next()) {
                long temp = result.getLong("id");
                if(temp > 0 && temp == id) {
                    unique = true;
                } else {
                    unique = false;
                }
            }
         } catch (SQLException e) {
        throw new AppException(ErrorCode.SQLException);
    }
    return unique;
   }
}

