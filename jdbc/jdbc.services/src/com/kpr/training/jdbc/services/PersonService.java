package com.kpr.training.jdbc.services;

/*
Requirement:
    To perform the CRUD operation of the Person.
 
Entity:
    1.Person
    2.PersonService
    3.AppException
    4.ErrorCode
 
Function declaration:
    public long create(Person person, Address address)
    public Person read(long id, boolean addressFlag)
    public ArrayList<Person> readAll()
    public void update(long id, Person person, Address address)
    public void delete(long id)

Jobs To Be Done:
    1. Create a Person.
    2. Read a record in the Person.
    3. Read all the record in the Person.
    4. Update a Person.
    5. Delete a Person.
*/



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.kpr.training.jdbc.Constant.QueryStatement;
import com.kpr.training.jdbc.Execption.AppException;
import com.kpr.training.jdbc.Execption.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

public class PersonService {

    @SuppressWarnings("static-access")
    public long create(Person person, Address address) {
        
        long noOfRowsAffected = 0;
        long personID = 0;
        AddressService addressService = new AddressService();
        
        ConnectionService jc = new ConnectionService();
        jc.init();
        
        PreparedStatement ps = null;

        if(checkUniqueEmail(person.getEmail(), ps, jc.con) == true) {
            long id = addressService.create(jc.con, address);
            
            try {
                
                ps = jc.con.prepareStatement(QueryStatement.createPersonQuery.toString(),
                        ps.RETURN_GENERATED_KEYS);
                ps.setString(1, person.getName());
                ps.setString(2, person.getEmail());
                ps.setDate(3, person.getBirthDate());
                ps.setLong(4, id);
                noOfRowsAffected = ps.executeUpdate();
                ResultSet personId = ps.getGeneratedKeys();
                
                while (personId.next()) {
                    personID = personId.getLong("GENERATED_KEY");
                }
                ps.close();
                
                if (noOfRowsAffected == 0) {
                    throw new AppException(ErrorCode.PERSON_CREATION_FAILS);
                }
                
                jc.con.commit();

            } catch (SQLException e) {
                try {
                    jc.con.rollback();
                } catch (SQLException ex) {
                    throw new AppException(ErrorCode.SQLException);
                }
                throw new AppException(ErrorCode.SQLException);
            }
        } else {
            throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
        }
        
        jc.release();
        
        return personID;
    }

    public Person read(long id, boolean addressFlag) {
        
        Person person = null;
        AddressService addressService = new AddressService();
        ConnectionService jc = new ConnectionService();
        jc.init();

        try {
            
            PreparedStatement ps;
            ps = jc.con.prepareStatement(QueryStatement.readPersonQuery.toString());
            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                person = new Person(result.getString("name"), result.getString("email"), result.getDate("birth_date"));
                person.setCreatedDate(result.getTimestamp("created_date"));
                person.setId(id);
                
                if (addressFlag) {
                    person.setAddress(addressService.read(jc.con, result.getLong("address_id")));
                }
            }
            ps.close();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
        
        jc.release();
        return person;
    }

    public ArrayList<Person> readAll() {
        
        ArrayList<Person> persons = new ArrayList<>(); 
        
        ConnectionService jc = new ConnectionService();
        jc.init();
        AddressService addressService = new AddressService();

        try {
            
            PreparedStatement ps;
            ps = jc.con.prepareStatement(QueryStatement.readAllPersonQuery.toString());
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                Person person = new Person(result.getString("name"), result.getString("email"),
                        result.getDate("birth_date"));
                Address address = addressService.read(jc.con, result.getLong("address_id"));
                person.setCreatedDate(result.getTimestamp("created_date"));
                person.setAddress(address);
                persons.add(person);
            }
            ps.close();
            
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }

        jc.release();
        return persons;
    }

    public void update(long id, Person person, Address address) {

        int numberOfRowsAffected = 0;
        
        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.POSTAL_CODE_ZERO);
        } else {
            ConnectionService jc = new ConnectionService();
            jc.init();
            PreparedStatement ps = null;

            if(checkEmailUniqueExceptID(id, person.getEmail(), ps, jc.con) == true) {

                try {
                    ps = jc.con.prepareStatement(QueryStatement.updatePersonQuery.toString());
                    ps.setString(1, address.getStreet());
                    ps.setString(2, address.getCity());
                    ps.setInt(3, address.getPostalCode());
                    ps.setString(4, person.getName());
                    ps.setString(5, person.getEmail());
                    ps.setDate(6, person.getBirthDate());
                    ps.setLong(7, id);
                    numberOfRowsAffected = ps.executeUpdate();
                    jc.con.commit();
                } catch (SQLException e) {
                    try {
                        jc.con.rollback();
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    throw new AppException(ErrorCode.SQLException);
                }
                if (numberOfRowsAffected == 0) {
                    throw new AppException(ErrorCode.PERSON_UPDATION_FAILS);
                } else {
                    jc.release();
                }
            } else {
                throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
            }
        }
    }

    public void delete(long id) {

        int numberOfRowsAffected = 0;
        ConnectionService jc = new ConnectionService();
        jc.init();
        
        try {
            
            PreparedStatement ps;
            ps = jc.con.prepareStatement(QueryStatement.deletePersonQuery.toString());
            ps.setLong(1, id);
            numberOfRowsAffected = ps.executeUpdate();
            ps.close();
            jc.con.commit();
            
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
        if (numberOfRowsAffected == 0) {
            throw new AppException(ErrorCode.PERSON_DELETION_FAILS);
        } else {
            jc.release();
        }
    }
    
    public boolean checkUniqueEmail(String email, PreparedStatement ps, Connection con) {
        
        ResultSet result;
        boolean unique = true;
        
        try {
            
            ps = con.prepareStatement(QueryStatement.emailUnique.toString());
            ps.setString(1, email);
            result = ps.executeQuery();
            
            if(result.next()) {
                if( ! (result.getLong("id") > 0 )) {
                    unique = true;
                } else {
                    unique = false;
                }
            }
            
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
        
        return unique;
    }
    
    public boolean checkEmailUniqueExceptID(long id, String email, PreparedStatement ps, Connection con) {
        
        ResultSet result;
        boolean unique = true;
        
        try {
            
            ps = con.prepareStatement(QueryStatement.emailUnique.toString());
            ps.setString(1, email);
            result = ps.executeQuery();
            
            if(result.next()) {
                long temp = result.getLong("id");
                if(temp > 0 && temp == id) {
                    unique = true;
                } else {
                    unique = false;
                }
            }
            
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
        return unique;
    }
}
