/*
 * Requirement:
 *      To create a POJO class for person.
 * 
 * Entity:
 *      Person
 * 
 * Method Signature:
 *      public Person(String firstName,String lastName, String email, Date birthDate, Timestamp createdDate)
 *      public String getfirstName()
 *      public String setfirstName()
 *      public String getlasName()
 *      public void setName(String lastName)
 *      public String getEmail()
 *      public void setEmail(String email)
 *      public Date getBirthDate()
 *      public void setBirthDate(Date birthDate)
 *      public String toString()
 *  
 * Jobs to be Done:
 *      1)Declare the fields as private.
 *      2) Create a constructor with four parameters which is the values of fields.
 *          2.1)Assign the values of fields and pass as argument values to the constructor.
 *      3) Create the method to get the firstName and lastName. 
 *      4) create the method to get the email.
 *      5) Create the method to get the birthDate.
 *      6) Create the method to assign the value of firstName and lastName.
 *      7) create the method to assign the value of email.
 *      8) Create the method to assign the value of city.
 *      9) Create the method to assign the value of birthDate.
 *      10) Override the toString method.
 * 
 * pseudo Code:

class Person {
    
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private Date birthDate;
    private Timestamp createdDate;
    private Address address;


    public Person(String firstName,String lastName, String email, Date birthDate) {
        this.firstName = firstName;
        this.setLastName(lastName);
        this.email = email;
        this.birthDate = birthDate;
    }

    public String getfirstName() {
        return firstName;
    }

    public void setName(String name) {
        this.firstName = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp created_date) {
        this.createdDate = created_date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName
                + ", email=" + email + ", birthDate=" + birthDate + ", createdDate=" + createdDate
                + ", address=" + address + "]";
    }

}

 */

package com.kpr.training.jdbc.model;

import java.sql.Timestamp;
import java.util.Date;

public class Person {
    
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private Date birthDate;
    private Timestamp createdDate;
    private Address address;


    public Person(String firstName,String lastName, String email, Date birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return new StringBuilder("Person [id=")
                .append(id)
                .append(", firstName=")
                .append(firstName)
                .append(", lastName=")
                .append(lastName)
                .append(", email=")
                .append(email)
                .append(", birthDate=")
                .append(birthDate)
                .append(", createdDate=")
                .append(createdDate)
                .append(", address=")
                .append(address)
                .append("]")
                .toString();
    }

}
