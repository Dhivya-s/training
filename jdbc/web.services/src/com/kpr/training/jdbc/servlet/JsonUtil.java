package com.kpr.training.jdbc.servlet;

import com.kpr.training.jdbc.model.Person;

//import org.json.simple.JSONObject;
//
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.kpr.training.jdbc.model.Person;
//import com.kpr.training.validator.Validator;

public class JsonUtil {
    
    public Object deSerialize(String personJson, Object typeObj) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        
        return gson.fromJson( personJson, typeObj.getClass());
    }
    
    public String serialize(Object typeObj) {
        
        Gson gson = new Gson();
        
        return gson.toJson(typeObj.getClass());
    }
    
    @SuppressWarnings({ "unchecked", "static-access" })
    public static void main(String[] args) {
        
        JsonUtil jsonUtil = new JsonUtil();
        
        String personJson = "{ 'firstName' : 'Shree ',"
                          + " 'lastName' : 'Dhushandhan',"
                          + " 'email' : 'dhushandhan.sd@gmail.com',"
                          + " 'birthDate' : '18-05-2001'}";
        
//      JsonUtil jsonUtil = new JsonUtil();
//      
//      Person personGot = jsonUtil.deSerialize(personJson);
//      
//      System.out.println(personGot.getFirstName());
//      System.out.println(personGot.getLastName());
//      System.out.println(personGot.getEmail());
//      System.out.println(personGot.getBirthDate());
//      
//      String personString = jsonUtil.serialize(personGot);
//      
//      System.out.println(personString);
        
//      JSONObject jsonObj = new JSONObject();
//      
//      jsonObj.put("firstName", "Dandana");
//      jsonObj.put("lastName", "nakaaa");
//      jsonObj.put("email", "dandanaka@gmail.com");
//      jsonObj.put("street", "dubakur Street");
//      jsonObj.put("city", "Bombay");
//      jsonObj.put("postalCode", "126983");
//      
//      String jsonString = jsonUtil.stringConverter(jsonObj);
//      
//      Person personGot = jsonUtil.classConverter(jsonString);
//      
//      personGot.setBirthDate(Validator.dateChanger("19-11-1999"));
//      
//      System.out.println(personGot.getFirstName());
//      System.out.println(personGot.getLastName());
//      System.out.println(personGot.getBirthDate());
        
//        Person person = new Person();
//        
//        person.setFirstName("Dhivya");
//        person.setLastName("S");
//        person.setEmail("sdhivya@gmail.com");
//        person.setBirthDate(null);
//        
//        
//        String jsonNew = jsonUtil.serialize(person);
//        
//        System.out.println(jsonNew);
        
    }

    public Person deSerialize(String string) {
        // TODO Auto-generated method stub
        return null;
    }
    
    

}
