package com.kpr.training.jdbc.servlet;



    import java.io.PrintWriter;

    import javax.servlet.http.HttpServlet;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;

    public class AddressServlet extends HttpServlet {
        
        private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest req, HttpServletResponse res) {
            
            try (PrintWriter writer = res.getWriter() ){
                
                res.setContentType("text/html");
                res.setCharacterEncoding("UTF-8");
                
                writer.append("<!DOCTYPE html>\r\n")
                   .append("<html>\r\n")
                   .append("        <head>\r\n")
                   .append("            <title>Address</title>\r\n")
                   .append("        </head>\r\n")
                   .append("        <body>\r\n")
                   .append("            <h2>Address</h2>")
                   .append("        </body>\r\n")
                   .append("</html>\r\n");
                
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            
        }

    }



