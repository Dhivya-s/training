package com.kpr.training.jdbc.servlet;

 /*
 PersonServlet
1) WBS for doPost():

Requirement:
    To implement the doPost method for PersonServlet.

Entity:
    PersonServlet

Method Signature:
    void doPost(HttpServiceRequest req, HttpServiceResponse res) {}

Jobs to be Done:
    1) Get the person from the http Request and storing it in Json String.
    2) Deserializing the Json String to Person type form and storing it in person.
    3) invoke the create method from personService with person as parameter and getting its id as long.
    4) Serializing the id to Json String and sending it as response to the httpResponse.

Pseudo Code:


2) WBS for doget() :

Requirement:
    To implement the doGet method for PersonServlet.

Entity:
    PersonServlet

Method Signature:
    void doGet(HttpServiceRequest req, HttpServiceResponse res) {}

Jobs to be Done:
    1) Get the person from the http Request and storing it in Json String.
    2) Deserializing the Json String to Person type form and storing it in person.
    3) invoke the read method from personService with person as parameter.
    4) Serializing the id to Json String and sending it as response to the httpResponse.

Pseudo Code:


3) WBS for doput();
Requirement:
    To implement the doPut method for PersonServlet.

Entity:
    PersonServlet

Method Signature:
    void doPut(HttpServiceRequest req, HttpServiceResponse res) {}
    
Jobs to be Done:
    1) Get the person from the http Request and storing it in Json String.
    2) Deserializing the Json String to Person type form and storing it in person.
    3) invoke the update method from personService with person as parameter.
    4) Sending the response to the http response about the updation.

4) WBS for doDelete():

Requirement:
    To implement the doDelete method for PersonServlet.

Entity:
    PersonServlet

Method Signature:
    void doDelete(HttpServiceRequest req, HttpServiceResponse res) {}

Jobs to be Done:
    1) Get the person from the http Request and storing it in Json String.
    2) Deserializing the Json String to Person type form and storing it in person.
    3) invoke the delete method from personService with person as parameter.
    4) Sending the response to the http response about the deletion.
     
*/


import java.io.BufferedReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.kpr.training.jdbc.model.Person;

//import org.json.simple.JSONObject;

import com.kpr.training.jdbc.service.PersonService;


public class PersonServlet extends HttpServlet {
    
    private static final long serialVersionUID = 1L;
    Person personClass = new Person();
    Person personGot;
    private static PersonService personService = new PersonService();
    private static JsonUtil jsonUtil = new JsonUtil();

    protected void doGet(HttpServletRequest req, HttpServletResponse res) {
        
        res.setContentType("text/html");
        res.setCharacterEncoding("UTF-8");
        
        try (PrintWriter writer = res.getWriter()) {
            
            BufferedReader reader = req.getReader();
            
            StringBuilder builder = new StringBuilder();
            
            String line = null;
            
            while((line = reader.readLine())!=null) {
                builder.append(line);
            }
            
            Person newPerson = (Person) jsonUtil.deSerialize(builder.toString(), personClass);
            
            Person personGot = personService.read(newPerson.getId(), true);
            
            writer.append(personGot.getFirstName()+" "+personGot.getLastName());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
    @SuppressWarnings("unchecked")
    protected void doPost(HttpServletRequest req, HttpServletResponse res) {
        
        try {
            BufferedReader reader = req.getReader(); 
            PrintWriter writer = res.getWriter();
            StringBuilder personJson = new StringBuilder();
            
            String line = null;
            
            while((line = reader.readLine())!= null) {
                personJson.append(line);
            }
            
            personGot = (Person) jsonUtil.deSerialize(personJson.toString(), personClass);
            
                        
            long personId = personService.create(personGot);
            
            if(personId != 0) {
                writer.append("Person Creation Success : " + personId);
            } else {
                writer.append("Person Creation Failure");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected void doPut(HttpServletRequest req, HttpServletResponse res) {
        
        try {
            
            PrintWriter writer = res.getWriter();
            
            BufferedReader reader = req.getReader();
            
            StringBuilder personJson = new StringBuilder();
            
            String line = null;
            
            while((line = reader.readLine())!= null) {
                personJson.append(line);
            }
            
            Person person = (Person) jsonUtil.deSerialize(personJson.toString(), personClass);
            
            personService.update(person);
            
            res.setContentType("text/html");
            res.setCharacterEncoding("UTF-8");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    protected void doDelete(HttpServletRequest req, HttpServletResponse res) {
        
        try {
            
            BufferedReader reader = req.getReader();
            
            StringBuilder personId = new StringBuilder();
            
            String line = null;
            
            while((line = reader.readLine()) != null) {
                personId.append(line);
            }
            
            Person person = (Person) jsonUtil.deSerialize(personId.toString(), personClass);
            
            personService.delete(person.getId());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

    

