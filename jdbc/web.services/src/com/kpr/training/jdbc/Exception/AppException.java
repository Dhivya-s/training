/*
  Requirement:

       To write the AppException for the person and address service

   Entity:
       AppException
       ErrorCode
 
  Method Signature:
       public AppException(ErrorCode code)
       public AppException(ErrorCode code, Exception e)
  
  Jobs To Be Done:
       1) The respective errors using its code message are declared in enum.
       2) The constructor is created with reference to error thrown from address and person service.
       3) Create AppException class which extends Runtime Exception 
       4) A constructor is created for AppException using getMessage() method
       4) The respective error code and message is printed.
  
   Pseudo code:
   
    enum ErrorCode {
        //create the respective code and its message.
        TYPE_OF_ERRORCODE("ERR401", "Error Message");
    }
    
    class AppException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;

    public AppException(ErrorCode code, Exception e) {
        super(code.getCode() + " : " + code.getMessage());
        e.printStackTrace();
    }
    
    public AppException(ErrorCode code) {
        super(code.getCode() + " : " + code.getMessage());
        System.out.println(
                code.getCode() + " : " + code.getMessage());
    }

}
 
 */
package com.kpr.training.jdbc.Exception;

public class AppException extends RuntimeException {

    private static final long serialVersionUID = 1L;
   
    public AppException(ErrorCode code, Exception e) {
        
        super(new StringBuilder (code.getCode())
                                 .append(" : " ) 
                                 .append(code.getMessage())
                                 .append(",")   
                                 .append(e)
                                 .toString());
                 
    }

    public AppException(ErrorCode code) {
        
        super(new StringBuilder (code.getCode())
                                .append(" : " ) 
                                .append(code.getMessage())
                                .append(",") 
                                .toString());
    }


}
