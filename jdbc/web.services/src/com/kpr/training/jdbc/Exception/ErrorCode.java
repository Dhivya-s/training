package com.kpr.training.jdbc.Exception;

public enum ErrorCode {

    INVALID_POSTAL_CODE ("101", "postal code should not be zero"),
    READING_ADDRESS_FAILED("102", "Failed to read address"),
    ADDRESS_CREATION_FAILED("103", "Failed to create Address"),
    ADDRESS_UPDATION_FAILED("104", "Failed to update Address"),
    ADDRESS_DELETION_FAILED("105", "Failed to delete Address"),
    PERSON_CREATION_FAILED("106", "Failed to createPerson"),
    PERSON_UPDATION_FAILED("107", "Failed to update Person"),
    PERSON_DELETION_FAILED("108", "Failed to delete Person"),
    CONNECTION_FAILED("109", "Failed to initiate connection"),
    EMAIL_NOT_UNIQUE("111", "Email already exists"),
    CONNECTION_FAILED_TO_CLOSE("112", "Failed to close the connection"),
    FAILED_TO_COMMIT_OR_ROLLBACK("113", "Failed to commit or rollBack"),
    FAILED_TO_CHECK_EMAIL("114", "Failed to check email"),
    READING_PERSON_FAILED("115", "Failed to read person"),
    READING_ADDRESSID_FAILED("116", "Failed to read address id"),
    FAILED_TO_CHECK_ADDRESS("117", "Failed to check address"),
    SEARCHING_ADDRESS_FAILED("118", "Failed to search address"),
    FAILED_TO_GET_ADDRESS_SIZE("119", "Failed to get size of address table"),
    FIRST_NAME_AND_LAST_NAME_DUPLICATE("120", "First name and last name already exists"),
    CHECKING_ADDRESS_USAGE_FAILED("121", "Failed to check address usage"),
    WRONG_DATE_FORMAT("122", "Enter valid date in valid format dd-mm-yyyy"),
    CHECKING_UNIQUE_NAME_FAILED("123", "Failed to check name is unique or not"),
    SETTING_VALUE_FAILED("124", "Failed to set value for Prepared Statement"),
    INVALID_FIRST_NAME("125", "First name should not be null or empty"),
    INVALID_LAST_NAME("126", "Last name should not be null or empty"),
    INVALID_EMAIL("127", "Email should not be null or empty");
   
    private final String code;
    private final String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
