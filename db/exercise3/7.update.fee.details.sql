UPDATE semester_fee  
   SET paid_year = (2020)
      ,paid_status = 'paid'
 WHERE stud_id = 21;
UPDATE semester_fee
   SET paid_status =
 (CASE stud_id WHEN 101 THEN 'paid'
               WHEN 102 THEN 'paid'
               WHEN 103 THEN 'paid'
               WHEN 104 THEN 'paid'
               WHEN 105 THEN 'paid'
  END)
      ,paid_year =
 (CASE stud_id WHEN 101 THEN (2020)
               WHEN 102 THEN (2020)
               WHEN 103 THEN (2020)
               WHEN 104 THEN (2020)
               WHEN 105 THEN (2020)
  END)
 WHERE stud_id = (101,102,103,104,105);
SELECT student.name        
      ,student.roll_number 
      ,semester_fee.paid_status
      ,semester_fee.paid_year
      ,semester_fee.amount
  FROM student
      ,semester_fee
 WHERE student.id = semester_fee.stud_id;