SELECT  student.name
       ,student.roll_number
       ,student.phone
       ,student.address
       ,student.dob
       ,student.email
       ,college.name
       ,university.university_name
  FROM  semester_result semester_result
  LEFT JOIN student student
    ON  semester_result.stud_id = student.id
  LEFT JOIN college college
    ON  college.id = student.college_id
  LEFT JOIN university university
    ON  university.univ_code = college.univ_code
 WHERE  semester_result.semester = 6
   AND  semester_result.gpa > 8
