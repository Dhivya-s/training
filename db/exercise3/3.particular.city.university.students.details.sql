SELECT student.roll_number
      ,student.name
      ,student. gender
      ,student.dob
      ,student. email
      ,student.phone
      ,student. address
      ,college.name
      ,department.dept_name
      ,designation.name AS hod_name 
  FROM student
      ,department
      ,college
      ,designation
      ,semester_result
      ,university
 WHERE student.college_id = college.id 
   AND university.univ_code = college.univ_code
   AND department.univ_code = university.univ_code
   AND student.id = semester_result.stud_id
   AND college.city = 'coimbatore'
   AND university.university_name = 'anna' 
 GROUP BY student.roll_number

 