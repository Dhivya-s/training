SELECT  employee.id 
       ,employee.name 
       ,employee.dob
       ,employee.email  
       ,employee.phone  
       ,college.name 
       ,department.dept_name
  FROM  employee  
       ,college
       ,department
       ,university
       ,designation
 WHERE  university.university_name = 'anna'
   AND  employee.college_id = college.id
   AND  university.univ_code = department.univ_code
   AND  designation.id = employee.desig_id
 GROUP BY college.name
 ORDER BY designation.rank
