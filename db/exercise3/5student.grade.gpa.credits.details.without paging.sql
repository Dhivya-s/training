SELECT student.id 
      ,student.roll_number
      ,student.name  
      ,student.dob 
      ,student.gender 
      ,student.email  
      ,student.address  
      ,student.academic_year
      ,college.name
      ,semester_result.semester
      ,semester_result.grade
      ,semester_result.credits
      ,semester_result.gpa  
  FROM student
      ,semester_result
      ,college
 WHERE semester_result.stud_id = student.id
   AND college.id = student.college_id
 ORDER BY college.name,semester_result.semester

