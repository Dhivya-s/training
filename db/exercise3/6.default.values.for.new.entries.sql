SELECT student.name
      ,student.roll_number 
      ,college.name
      ,university.university_name
      ,semester_fee.semester
      ,semester_fee.paid_year
      ,semester_fee.paid_status
      ,semester_fee.amount
  FROM student
      ,university
      ,college
      ,semester_fee
 WHERE semester_fee.stud_id = student.id
   AND university.univ_code = college.univ_code 
   AND semester_fee.semester = 1
 GROUP BY  student.roll_number
         

