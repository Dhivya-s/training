SELECT  student.name
       ,student.roll_number
       ,student.phone
       ,student.address
       ,college.name
       ,university.university_name
       ,semester_fee.semester
  FROM  semester_fee semester_fee
  LEFT JOIN student student
    ON  semester_fee.stud_id = student.id
  LEFT JOIN college_department college_department 
    ON  college_department.cdept_id = semester_fee.cdept_id
  LEFT JOIN college college
    ON  college.id = student.college_id
  LEFT JOIN university university
    ON  university.univ_code = college.univ_code
 WHERE  semester_fee.paid_status = 'unpaid'
   AND  semester_fee.paid_year IS NULL