UPDATE semester_fee
   SET paid_status = 'Paid'
      ,paid_year = '2020'
 WHERE stud_id = (SELECT student.id
                    FROM student
                   WHERE student.roll_number = '18ee101');
                    
UPDATE semester_fee
   SET paid_status = 'Paid'
      ,paid_year = 2020
 WHERE stud_id IN (SELECT student.id
                     FROM student 
                    WHERE roll_number IN ('18ee102'
                                         ,'18ee103'
                                         ,'18ee104'
                                         ,'18ee105'));
SELECT student.name        
      ,student.roll_number 
      ,semester_fee.paid_status
      ,semester_fee.paid_year
      ,semester_fee.amount
  FROM student
      ,semester_fee
 WHERE student.id = semester_fee.stud_id;