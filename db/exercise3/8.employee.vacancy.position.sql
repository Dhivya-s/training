SELECT designation.id
      ,designation.rank
      ,designation.name AS designation_name
      ,college.name AS college_name
      ,department.dept_name
      ,university.university_name
  FROM professor_syllabus professor_syllabus
 INNER JOIN syllabus syllabus 
    ON professor_syllabus.syllabus_id = syllabus.id
 INNER JOIN college_department college_department
    ON syllabus.cdept_id = college_department.cdept_id
 INNER JOIN department department 
    ON college_department.udept_code = department.dept_code
 INNER JOIN employee employee
    ON college_department.college_id = employee.college_id
 INNER JOIN designation designation
    ON employee.desig_id = designation.id
 INNER JOIN college college
    ON college_department.college_id = college.id
 INNER JOIN university university
    ON college.univ_code = university.univ_code
 WHERE employee.id IS NULL
 GROUP BY desig_id