SELECT college.code
      ,college.name
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.dept_name
      ,employee.name
  FROM college college
  LEFT JOIN university university
    ON college.univ_code = university.univ_code
  RIGHT JOIN department department
    ON university.univ_code = department.univ_code
  LEFT JOIN employee employee 
    ON college.id = employee.college_id
  LEFT JOIN designation designation
    ON designation.id = employee.desig_id
 WHERE designation.name = 'hod'
HAVING department.dept_name IN ('CSE','IT');