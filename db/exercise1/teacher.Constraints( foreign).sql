ALTER TABLE `teacher` 
ADD CONSTRAINT `time_table_id`
  FOREIGN KEY (`time_table_id`)
  REFERENCES `time_table` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;