CREATE TABLE `staff` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `age` INT NOT NULL,
  `subject` VARCHAR(45) NOT NULL,
  `class_handling` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
