SELECT MIN(salary) AS minsalary
      ,MAX(salary) AS maxsalary
      ,AVG(salary) AS avgsalary
      ,COUNT(staff_id) AS id
      ,SUM(salary) AS total
      ,NOW() AS time_date
 FROM teacher;    